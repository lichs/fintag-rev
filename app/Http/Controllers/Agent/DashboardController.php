<?php

namespace App\Http\Controllers\Agent;

use App\User;
use App\Agent;
use App\RequestAgent;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:agent');
    }

    public function getProfilePage()
    {
      return view('agent.page.profile');
    }

    public function getPermintaanPengajuanPage(Request $request)
    {
      $agents = Agent::where('user_id', $request->user()->id)->get();

      $provinsi = DB::table('provinsi')->get();

      foreach ($agents as $agent)
        {
          $countRequest = RequestAgent::with('pelakuUsaha')->where('agent_id', $agent->id)
                                                            ->where('accepted', 0)->count();
        }

      return view('agent.page.permintaanPengajuan', ['countRequest' => $countRequest, 'provinsis' => $provinsi]);
    }

    public function getDashboardPage(Request $request)
    {
      $user = User::find($request->user()->id);

      $agents = $user->agent;

      if ($user->agent()->get()->isEmpty())
      {

        $provinsi = DB::table('provinsi')->get();

        return view('agent.isi_profile',['provinsis' => $provinsi]);
      }

      return view('agent.dashboard');
    }

    public function getSertifikatPage()
    {
      return view('agent.sertifikat');
    }

    public function getDataPengajuanPage()
    {
      return view('agent.page.data_pengajuan');
    }

    public function getDompetAgentPage()
    {
      return view('agent.page.dompet');
    }

    public function getPointPage()
    {
      return view('agent.page.point');
    }
}
