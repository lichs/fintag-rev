<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'agent'], function(){
    Route::group(['prefix' => 'data'], function(){
        Route::get('/kabupaten/{id}', 'Agent\DataController@getKabupatenByProvinsi');
        Route::get('/kecamatan/{id}', 'Agent\DataController@getKecamatanByKabupaten');
        Route::get('/kelurahan/{id}', 'Agent\DataController@getKelurahanByKecamatan');
        Route::get('/request-agent', 'Agent\DataController@getRequestAgent');
        Route::get('/detail-request/{id}', 'Agent\DataController@getDetailsRequestPengajuan');
        Route::get('/history-request', 'Agent\DataController@getRequestHistory');
        Route::get('/cek-nik/{nik}', 'Agent\DataController@getPuByNik');
        Route::get('/agent/{id}', 'Agent\DataController@getAllAgentData');
        Route::get('/data-pengajuan', 'Agent\DataController@getRequestAccepted');
    });

    Route::get('/', ['as' => 'agent.dashboard', 'uses' => 'Agent\DashboardController@getDashboardPage']);
    Route::get('/profile', ['as' => 'agent.profile', 'uses' => 'Agent\DashboardController@getProfilePage']);
    route::post('/profile/{id}', 'Agent\AgentController@updateAgent');

    Route::get('/permintaan-pengajuan', ['as' => 'agent.permintaan-pengajuan', 'uses' => 'Agent\DashboardController@getPermintaanPengajuanPage']);

    Route::post('/permintaan-pengajuan', 'Agent\AgentController@createPermintaanPengajuan');

    Route::get('/permintaan-pengajuan/terima/{id}', ['uses' => 'Agent\RequestAgentController@terimaPermintaanPengajuan']);

    Route::get('/permintaan-pengajuan/tolak/{id}', ['uses' => 'Agent\RequestAgentController@tolakPermintaanPengajuan']);

    Route::get('/data-pengajuan', ['as' => 'agent.data-pengajuan', 'uses' => 'Agent\DashboardController@getDataPengajuanPage']);

    Route::get('/dompet', ['as' => 'agent.dompet', 'uses' => 'Agent\DashboardController@getDompetAgentPage']);

    Route::get('/point', ['as' => 'agent.point', 'uses' => 'Agent\DashboardController@getPointPage']);

    Route::get('/submit-pengajuan-badan-usaha/{request_id}', ['as' => 'agent.pengajuan-badan-usaha', 'uses' => 'Agent\PengajuanPuController@createPengajuanBadanUsaha']);
    Route::get('/submit-pengajuan-perorangan/{request_id}', ['as' => 'agent.pengajuan-perorangan', 'uses' => 'Agent\PengajuanPuController@createPengajuanPerorangan']);

    Route::group(['prefix' => 'pengajuan-perorangan'], function(){
        Route::get('/{id}', ['as' => 'agent.pengajuan-perorangan', 'uses' => 'Agent\PengajuanPuController@getPengajuanPeroranganPage']);
        Route::get('/profile-pribadi/{pengajuan_id}/{pelaku_usaha_id}', 'Agent\PengajuanPuController@getProfilePribadi');
        Route::get('/profile-usaha/{pengajuan_id}', 'Agent\PengajuanPuController@getProfileUsahaPage');
        Route::get('/profile-keuangan/{pengajuan_id}', 'Agent\PengajuanPuController@getProfileKeuanganPage');
        Route::get('/profile-pembiayaan/{pengajuan_id}', 'Agent\PengajuanPuController@getProfilePembiayaan');
        Route::get('/persyaratan-dokumen/{pengajuan_id}', 'Agent\PengajuanPuController@getPersyaratanDokumenPage');

        Route::post('/profile-pribadi', 'Agent\PengajuanPuController@submitProfilePribadi');
    });

    Route::get('/pengajuan-badan-usaha/{id}', ['as' => 'agent.pengajuan-badan-usaha', 'uses' => 'Agent\PengajuanPuController@getPengajuanBadanUsahaPage']);
    Route::get('/pengajuan/profile-badan-usaha', 'Agent\PengajuanPuController@getProfileBadanUsahaPage');
    Route::get('/pengajuan/profile-pribadi', 'Agent\PengajuanPuController@getProfilePribadi');

    Route::post('/isi-profile', 'Agent\AgentController@createAgent');
    Route::get('/sertifikat', 'Agent\DashboardController@getSertifikatPage');
    Route::post('/sertifikat', 'Agent\AgentController@createSertifikatAgent');

    Route::get('/login', [ 'as' => 'agent.login' ,'uses' => 'Agent\Auth\LoginController@showLoginForm']);

    Route::post('/login', 'Agent\Auth\LoginController@login');
    Route::post('/logout', 'Agent\Auth\LoginController@logout');

    Route::get('/register', [ 'as' => 'agent.register' ,'uses' => 'Agent\Auth\RegisterController@showRegistrationForm']);
    Route::post('/register', 'Agent\Auth\RegisterController@register' );
});

Route::group(['prefix' => 'admin'], function(){

});

Route::group(['prefix' => 'pu'], function(){

});

//Auth::routes();

Route::get('/home', 'HomeController@index');
