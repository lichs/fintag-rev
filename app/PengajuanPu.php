<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PengajuanPu extends Model
{
    protected $table = 'pengajuan_pu';

    protected $fillable = [
        'request_id', 'jenis_usaha_id', 'jenis_pengajuan_id', 'jenis_kepemilikan_usaha_id',
        'agent_id', 'pelaku_usaha_id', 'tanggal_pengajuan', 'tanggal_selesai', 'tanggal_kelengkapan_data', 'jam_awal_pengajuan', 'kode', 'status', 'file_proposal',
        'keterangan_proposal', 'keterangan_pengajuan', 'status_respon_pengajuan',
        'status_respon_kelengkapan_data', 'status_respon_pilih_pp'
    ];

    public function request()
    {
        return $this->belongsTo('App\RequestAgent');
    }

    public function jenisUsaha()
    {
        return $this->belongsTo('App\JenisUsaha');
    }

    public function jenisPengajuan()
    {
        return $this->hasOne('App\JenisPengajuan', 'id', 'jenis_pengajuan_id');
    }

    public function jenisKepemilikanUsaha()
    {
        return $this->belongsTo('App\JenisKepemilikanUsaha');
    }

    public function agent()
    {
        return $this->belongsTo('App\Agent');
    }

    public function pelakuUsaha()
    {
        return $this->belongsTo('App\PelakuUsaha');
    }

    public function getJenisUsaha()
    {
        foreach ($this->jenisPengajuan as $jenisPengajuan)
        {
            return $jenisPengajuan->nama;
        }
    }

    public function identitasPengajuanPerorangan()
    {
        return $this->hasMany(IdentitasPengajuanPerorangan::class, 'pengajuan_id', 'id');
    }
}
