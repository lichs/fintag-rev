<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Agent extends Model
{
    protected $table = 'agents';

    protected $fillable = [
    	'user_id', 'nama', 'no_telp', 'user_id','jenis_kelamin', 'tempat_lahir', 'tanggal_lahir', 'provinsi','kota',
    	'kelurahan', 'kecamatan','alamat', 'kode_pos', 'agent_image', 'nik',
      'provinsi_ktp' ,'kota_ktp', 'kelurahan_ktp',
      'kecamatan_ktp', 'alamat_ktp', 'kodepos_ktp', 'ktp_image', 'aktif', 'validate'
    ];

    public function requestAgent()
    {
      return $this->hasMany(RequestAgent::class);
    }

    public function user()
    {
    	return $this->belongsTo(User::class);
    }

    public function sertifikat()
    {
      return $this->hasMany(Sertifikat::class);
    }

    public function is_validate()
    {
    	return (bool) $this->validate;
    }

    public function setNamaAttribute($value)
    {
      $this->attributes['nama'] = ucfirst($value);
    }

    public function setTempatLahirAttribute($value)
    {
      $this->attributes['tempat_lahir'] = ucfirst($value);
    }

    public function setAlamatAttribute($value)
    {
      $this->attributes['alamat'] = ucfirst($value);
    }

    public function setAlamatKtpAttribute($value)
    {
      $this->attributes['alamat_ktp'] = ucfirst($value);
    }
}
