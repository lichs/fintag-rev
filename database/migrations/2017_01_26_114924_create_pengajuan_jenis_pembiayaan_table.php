<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePengajuanJenisPembiayaanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengajuan_jenis_pembiayaan', function(Blueprint $table){
            $table->increments('id');
            $table->integer('pengajuan_id')->unsigned();
            $table->integer('jenis_pembiayaan_id')->unsigned();
            $table->timestamps();

            $table->foreign('pengajuan_id')->references('id')->on('pengajuan_pu');
            $table->foreign('jenis_pembiayaan_id')->references('id')->on('jenis_pembiayaan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
