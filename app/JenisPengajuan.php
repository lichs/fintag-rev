<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JenisPengajuan extends Model
{
    protected $table = 'jenis_pengajuan';

    protected $fillable = ['nama'];

    public function pengajuanPu()
    {
        return $this->belongsTo('App\PengajuanPu');
    }
}
