@extends('layouts.app')

@section('content')
	<div id="container" class="effect aside-float aside-bright mainnav-lg">
		@foreach($agents as $agent)
			<div class="boxed">
				@include('agent.navigation')
					<div id="content-container">
					  <div id="page-content">
              <div class="panel">
              	<div class="panel-body pad-no">
                    <h3 class="panel-title">Form Pengajuan Badan Usaha</h3>
                    <ol class="breadcrumb">
                      <li class="home">Beranda</li>
                      <li><a href="#">Pendataan Pengajuan</a></li>
                      <li>Form Pengajuan Badan Usaha</li>
                      <li class="active">Profil Pembiayaan</li>
                    </ol>
              	</div>
                <div class="panel-body">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam mauris eros, dictum id nisi convallis, efficitur ultrices mauris. In condimentum eu risus eu gravida. In hac habitasse platea dictumst. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Suspendisse finibus mollis magna, at porttitor felis dictum et. Nulla sit amet est velit. Donec malesuada urna vel nisl faucibus, sed laoreet urna porta. </p>
                </div>
              </div>
              <div class="row">
                  <div class="col-lg-12">
                      <div class="panel">
                          <div class="panel-heading">
                              <h3 class="panel-title">Profile Pembiayaan</h3>
                          </div>
                          <form class="panel-body form-horizontal" action="index.html" method="post">
                              <div class="col-sm-12">
                                  <h3 class="h5 mar-ver"><span>Jenis Pembiayaan</span></h3>
                                  <div class="pad-hor">
                                    <div class="col-sm-10">
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" >Jenis Pembiayaan *</label>
                                            <div class="col-md-8">
                                                <div class="radio">
                                                    <input id="rbtJenisPembiayaan-1" class="magic-radio" type="radio" name="rbtJenisPembiayaan" value="Barang" onClick="setformDataJenisPembiayaan(this.value)" checked>
                                                    <label for="rbtJenisPembiayaan-1">Barang</label>
                                                    <input id="rbtJenisPembiayaan-2" class="magic-radio" type="radio" name="rbtJenisPembiayaan" value="Modal Kerja" onClick="setformDataJenisPembiayaan(this.value)">
                                                    <label for="rbtJenisPembiayaan-2">Modal Kerja</label>
                                                    <input id="rbtJenisPembiayaan-3" class="magic-radio" type="radio" name="rbtJenisPembiayaan" value="Jasa" onClick="setformDataJenisPembiayaan(this.value)">
                                                    <label for="rbtJenisPembiayaan-3">Jasa</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-sm-10 field-group" id="formJenisPembiayaanBarang">
                                        <h3 class="h6 mar-ver"><span>Jenis Pembiayaan Barang</span></h3>
                                        <div class="col-sm-10">
                                            <div class="form-group">
                                                <label class="col-md-4 control-label" >Merk *</label>
                                                <div class="col-md-5">
                                                    <input type="text"  class="form-control" >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-10">
                                            <div class="form-group">
                                                <label class="col-md-4 control-label" >Type/Model *</label>
                                                <div class="col-md-5">
                                                    <input type="text"  class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-10">
                                            <div class="form-group">
                                                <label class="col-md-4 control-label" >Jumlah *</label>
                                                <div class="col-md-2">
                                                    <input type="number" min="0"  class="form-control text-right" >
                                                </div>
                                                <div class="col-md-3"> unit</div>
                                            </div>
                                        </div>
                                        <div class="col-sm-10">
                                            <div class="form-group">
                                                <label class="col-md-4 control-label" >Obyek Pembiayaan *</label>
                                                <div class="col-md-6">
                                                    <div class="radio">
                                                        <input id="rbtOP-1" class="magic-radio" type="radio" name="rbtOP"  value="Baru">
                                                        <label for="rbtOP-1">Baru</label>
                                                        <input id="rbtOP-2" class="magic-radio" type="radio" name="rbtOP"  value="Sale &amp; Lease Back" checked>
                                                        <label for="rbtOP-2">Sale &amp; Lease Back</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-10">
                                            <div class="form-group">
                                                <label class="col-md-4 control-label" >Harga (termasuk PPN) Rp. *</label>
                                                <div class="col-md-5">
                                                    <input type="number" min="0"  class="form-control  text-right" >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-10">
                                            <div class="form-group">
                                                <label class="col-md-4 control-label" >Tujuan Pembiayaan *</label>
                                                <div class="col-md-8">
                                                    <input type="text"  class="form-control" >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-10">
                                            <div class="form-group">
                                                <label class="col-md-4 control-label" >Supplier/ Penjual *</label>
                                                <div class="col-md-8">
                                                    <input type="text"  class="form-control" >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-10">
                                            <div class="form-group">
                                                <label class="col-md-4 control-label" >Alamat Supplier *</label>
                                                <div class="col-md-8">
                                                    <input type="text"  class="form-control" >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-10">
                                            <div class="form-group">
                                                <label class="col-md-4 control-label" >Nomor Telepon/ Fax *</label>
                                                <div class="col-md-5">
                                                    <input type="text"  class="form-control" >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-10">
                                            <div class="form-group">
                                                <label class="col-md-4 control-label" >Contact Person *</label>
                                                <div class="col-md-5">
                                                    <input type="text"  class="form-control" >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mar-ver"></div>
                                    </div>

                                    <div class="col-sm-10 field-group" id="formJenisPembiayaanModalKerja">
                                        <h3 class="h6 mar-ver"><span>Jenis Pembiayaan Modal Kerja</span></h3>
                                        <div class="col-sm-10">
                                            <div class="form-group">
                                                <label class="col-md-4 control-label" >Peruntukan Modal Kerja *</label>
                                                <div class="col-md-8">
                                                    <input type="text"  class="form-control" >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-10">
                                            <div class="form-group">
                                                <label class="col-md-4 control-label" >Harga Total Rp. *</label>
                                                <div class="col-md-5">
                                                    <input type="number" min="0"  class="form-control  text-right" >
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-10 field-group" id="formJenisPembiayaanJasa">
                                        <h3 class="h6 mar-ver"><span>Jenis Pembiayaan Jasa</span></h3>
                                        <div class="col-sm-10">
                                            <div class="form-group">
                                                <label class="col-md-4 control-label" >Jasa yang dibutuhkan *</label>
                                                <div class="col-md-8">
                                                    <input type="text"  class="form-control" >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-10">
                                            <div class="form-group">
                                                <label class="col-md-4 control-label" >Harga Rp. *</label>
                                                <div class="col-md-5">
                                                    <input type="number" min="0"  class="form-control  text-right" >
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                  </div>
                              </div>

                              <div class="col-sm-12">
                                  <h3 class="h5 mar-ver"><span>Persyaratan Pembiayaan</span></h3>
                                  <div class="pad-hor">
                                      <div class="col-sm-10">
                                          <div class="form-group">
                                              <label class="col-md-4 control-label" >Jumlah Pembiayaan Rp. *</label>
                                              <div class="col-md-5">
                                                  <input type="number" min="0"  class="form-control   text-right" >
                                              </div>
                                          </div>
                                      </div>
                                      <div class="col-sm-10">
                                          <div class="form-group">
                                              <label class="col-md-4 control-label" >Uang Muka Rp. *</label>
                                              <div class="col-md-5">
                                                  <input type="number" min="0"  class="form-control   text-right" >
                                              </div>
                                          </div>
                                      </div>
                                      <div class="col-sm-10">
                                          <div class="form-group">
                                              <label class="col-md-4 control-label" >Pola Pembayaran *</label>
                                              <div class="col-md-6">
                                                  <div class="radio">
                                                  <input id="rbtPolaBayar-1" class="magic-radio" type="radio" name="rbtPolaBayar"  value="Lunas" checked>
                                                  <label for="rbtPolaBayar-1">Lunas</label>
                                                  <input id="rbtPolaBayar-2" class="magic-radio" type="radio" name="rbtPolaBayar"  value="Cicilan">
                                                  <label for="rbtPolaBayar-2">Cicilan</label>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="col-sm-10">
                                          <div class="form-group">
                                              <label class="col-md-4 control-label" >Suku Bunga *</label>
                                              <div class="col-md-6">
                                                  <div class="radio">
                                                  <input id="rbtSukuBunga-1" class="magic-radio" type="radio" name="rbtSukuBunga"  value="Fixed(Tetap)" checked>
                                                  <label for="rbtSukuBunga-1">Fixed(Tetap)</label>
                                                  <input id="rbtSukuBunga-2" class="magic-radio" type="radio" name="rbtSukuBunga"  value="Floating(Mengambang)">
                                                  <label for="rbtSukuBunga-2">Floating(Mengambang)</label>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="col-sm-10">
                                          <div class="form-group">
                                              <label class="col-md-4 control-label" >Tujuan Pemakaian Obyek *</label>
                                              <div class="col-md-6">
                                                  <div class="radio">
                                                  <input id="rbtPemakaianObyek-1" class="magic-radio" type="radio" name="rbtPemakaianObyek"  value="Sewa" checked>
                                                  <label for="rbtPemakaianObyek-1">Sewa</label>
                                                  <input id="rbtPemakaianObyek-2" class="magic-radio" type="radio" name="rbtPemakaianObyek"  value="Operasional">
                                                  <label for="rbtPemakaianObyek-2">Operasional</label>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="col-sm-10">
                                          <div class="form-group">
                                              <label class="col-md-4 control-label" >Jangka Waktu *</label>
                                              <div class="col-md-2">
                                              <input type="number" min="0"  class="form-control   text-right" >
                                              </div>
                                              <div class="col-md-3"> bulan</div>
                                          </div>
                                      </div>
                                      <div class="col-sm-10">
                                          <div class="form-group">
                                              <label class="col-md-4 control-label" >Pembayaran Angsuran Rp. *</label>
                                              <div class="col-md-5">
                                                  <input type="number" min="0"  class="form-control   text-right">
                                              </div>
                                          </div>
                                      </div>
                                      <div class="col-sm-10">
                                          <div class="form-group">
                                              <label class="col-md-4 control-label" >Cara Pembayaran *</label>
                                              <div class="col-md-4">
                                                  <select class="selectpicker form-control">
                                                      <option value="Transfer">Transfer</option>
                                                      <option value="STO/SI">STO/SI</option>
                                                      <option value="Cek/Giro">Cek/Giro</option>
                                                      <option value="Lain-Lain">Lain-Lain</option>
                                                  </select>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="col-sm-10">
                                          <div class="form-group">
                                              <label class="col-md-4 control-label" >Persyaratan Lain *</label>
                                              <div class="col-md-8">
                                                <input type="text"  class="form-control" >
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                              </div>

                              <div class="col-sm-12">
                                  <h3 class="h5 mar-ver"><span>Jaminan Kolateral</span></h3>
                                  <div class="pad-hor">
                                      <div class="col-sm-4 mar-ver">
                                      	<button class="btn btn-primary btn-hover-mint"><i class="fa fa-plus-circle"></i> Tambah Jaminan</button>
                                      </div>
                                      <div class="col-sm-10">
                                          <div class="form-group">
                                              <label class="col-md-4 control-label" >File Kolateral *</label>
                                              <div class="col-md-5">
                                                  <input type="file" id="file_koleteral" class="form-control" name="file_koleteral"  />
                                              </div>
                                          </div>
                                      </div>
                                      <div class="col-sm-10">
                                          <div class="form-group">
                                              <label class="col-md-4 control-label" >Keterangan *</label>
                                              <div class="col-md-8">
                                                  <input type="text"  class="form-control" >
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                              </div>

                              <div class="col-sm-12">
                                  <h3 class="h5 mar-ver"><span>Data Proposal</span></h3>
                                  <div class="pad-hor">
                                      <div class="col-sm-10">
                                          <div class="form-group">
                                              <label class="col-md-4 control-label" >File Proposal *</label>
                                              <div class="col-md-5">
                                                  <input type="file" id="file_proposal" class="form-control" name="file_proposal"  />
                                              </div>
                                          </div>
                                      </div>
                                      <div class="col-sm-10">
                                          <div class="form-group">
                                              <label class="col-md-4 control-label" >Keterangan *</label>
                                              <div class="col-md-8">
                                                  <input type="text"  class="form-control" >
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                              </div>

                              <div class="col-sm-12">
                                  <h3 class="h5 mar-ver"><span>Persyaratan Dokumen</span></h3>
                                  <div class="pad-hor">
                                      <div class="col-sm-10">
                                          <div class="form-group">
                                              <label class="col-md-4 control-label" >KTP Pemohon &amp; Pasangan (Istri/Suami) *</label>
                                              <div class="col-md-4">
                                                  <div class="radio">
                                                      <input id="rbtDoku1-1" class="magic-radio" type="radio" name="rbtDoku1"  onClick="setformDoku1(this.value)" value="Ada" checked>
                                                      <label for="rbtDoku1-1">Ada</label>
                                                      <input id="rbtDoku1-2" class="magic-radio" type="radio" name="rbtDoku1"  onClick="setformDoku1(this.value)" value="Tidak">
                                                      <label for="rbtDoku1-2">Tidak</label>
                                                  </div>
                                              </div>
                                              <div class="col-md-5" id="formDoku1">
                                                  <input type="file" id="file_doku1" class="form-control" name="file_doku1"  />
                                              </div>
                                          </div>
                                      </div>

                                      <div class="col-sm-10">
                                          <div class="form-group">
                                              <label class="col-md-4 control-label" >KTP Direktur &amp; Komisaris *</label>
                                              <div class="col-md-4">
                                                  <div class="radio">
                                                      <input id="rbtDoku2-1" class="magic-radio" type="radio" name="rbtDoku2"  onClick="setformDoku2(this.value)"  value="Ada" checked>
                                                      <label for="rbtDoku2-1">Ada</label>
                                                      <input id="rbtDoku2-2" class="magic-radio" type="radio" name="rbtDoku2"   onClick="setformDoku2(this.value)"  value="Tidak">
                                                      <label for="rbtDoku2-2">Tidak</label>
                                                  </div>
                                              </div>
                                              <div class="col-md-5" id="formDoku2">
                                                  <input type="file" id="file_doku2" class="form-control" name="file_doku2"/>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                              </div>

                              <div class="col-sm-12 mar-ver">
                                  <div class="col-sm-4">
                                      <a class="btn btn-default btn-block mar-ver" href="{{ redirect()->back()->getTargetUrl()}}">Tutup Halaman ini</a>
                                  </div>
                                  <div class="col-sm-4">
                                  	  <button class="btn btn-primary btn-block mar-ver" type="submit">Simpan</button>
                                  </div>
                              </div>
                          </form>
                      </div>
                  </div>
              </div>
            </div>
          </div>
				@include('agent.sidebar')
			</div>
		@endforeach
	</div>
@endsection

@push('script')
  <script>
    $( document ).ready(function() {
      $('.dtpicker').datepicker({
        dateFormat : "dd/mm/yy"
      });
    $('.selectpicker').selectpicker('refresh');
    $('#formJenisPembiayaanBarang').show();
    $('#formJenisPembiayaanModalKerja').hide();
    $('#formJenisPembiayaanJasa').hide();
    });
    function setformDataJenisPembiayaan(val){
    if (val=='Barang') {
    $('#formJenisPembiayaanBarang').show();
    $('#formJenisPembiayaanModalKerja').hide();
    $('#formJenisPembiayaanJasa').hide();
    }else if (val=='Modal Kerja'){
    $('#formJenisPembiayaanBarang').hide();
    $('#formJenisPembiayaanModalKerja').show();
    $('#formJenisPembiayaanJasa').hide();
    }else if (val=='Jasa'){
    $('#formJenisPembiayaanBarang').hide();
    $('#formJenisPembiayaanModalKerja').hide();
    $('#formJenisPembiayaanJasa').show();
    }
    }
    function setformDoku1(val){
    if (val=='Ada') {
    $('#formDoku1').show();
    }else{
    $('#formDoku1').hide();
    }
    }
    function setformDoku2(val){
    if (val=='Ada') {
    $('#formDoku2').show();
    }else{
    $('#formDoku2').hide();
    }
    }
  </script>
@endpush
