<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name') }}</title>

        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>
        <!--Bootstrap Stylesheet [ REQUIRED ]-->
        <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
        <!--Nifty Stylesheet [ REQUIRED ]-->
        <link href="{{ asset('css/nifty.min.css') }}" rel="stylesheet">
        <!--Nifty Premium Icon [ DEMONSTRATION ]-->
        <link href="{{ asset('css/demo/nifty-demo-icons.min.css')}}" rel="stylesheet">
        <!--Demo [ DEMONSTRATION ]-->
        <link href="{{ asset('css/demo/nifty-demo.min.css')}}" rel="stylesheet">
        <!--Magic Checkbox [ OPTIONAL ]-->
        <link href="{{ asset('plugins/magic-check/css/magic-check.min.css')}}" rel="stylesheet">
        <!--Pace - Page Load Progress Par [OPTIONAL]-->
        <link href="{{ asset('plugins/pace/pace.min.css')}}" rel="stylesheet">

        <link href="{{ asset('plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">

        <!--Bootstrap Table [ OPTIONAL ]-->
        <link href="{{ asset('plugins/bootstrap-table/bootstrap-table.min.css')}}" rel="stylesheet">

        <link href="{{ asset('plugins/datatables/media/css/dataTables.bootstrap.css') }}" rel="stylesheet">

        <link href="{{ asset('plugins/datatables/extensions/Responsive/css/dataTables.responsive.css')}}" rel="stylesheet">

        <link href="{{ asset('plugins/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet">
        <link href="{{ asset('plugins/bootstrap-select/bootstrap-select.min.css') }}" rel="stylesheet">

        <link href="{{ asset('plugins/select2/css/select2.css')}}" rel="stylesheet">

        <link href="{{ asset('premium/icon-sets/icons/solid-icons/premium-solid-icons.min.css')}}" rel="stylesheet">

        <!-- Fintag Custom theme -->
        <link href="{{ asset('css/custom.css')}}" rel="stylesheet">

        @stack('style')

        <script>
            window.Laravel = <?php echo json_encode([
                'csrfToken' => csrf_token(),
            ]); ?>
        </script>

    </head>
    <body>
        @yield('content')

        <!--Pace - Page Load Progress Par [OPTIONAL]-->
        <script src="{{ asset('plugins/pace/pace.min.js')}}"></script>
        <!--jQuery [ REQUIRED ]-->
        <script src="{{ asset('js/jquery-2.2.4.min.js')}}"></script>
        <!--BootstrapJS [ RECOMMENDED ]-->
        <script src="{{ asset('js/bootstrap.min.js')}}"></script>
        <!--NiftyJS [ RECOMMENDED ]-->
        <script src="{{ asset('js/nifty.min.js')}}"></script>
        <!--Demo script [ DEMONSTRATION ]-->
        <script src="{{ asset('js/demo/nifty-demo.min.js')}}"></script>
        <!--Bootstrap Table Sample [ SAMPLE ]-->
        <script src="{{ asset('js/demo/tables-bs-table.js')}}"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.0.6/handlebars.min.js"></script>

        <!--X-editable [ OPTIONAL ]-->
        <script src="{{ asset('plugins/x-editable/js/bootstrap-editable.min.js')}}"></script>
        <!--Bootstrap Table [ OPTIONAL ]-->
        <script src="{{ asset('plugins/bootstrap-table/bootstrap-table.min.js')}}"></script>
        <script src="{{ asset('plugins/datatables/media/js/jquery.dataTables.js')}}"></script>
        <script src="{{ asset('plugins/datatables/media/js/dataTables.bootstrap.js')}}"></script>
        <script src="{{ asset('plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js')}}"></script>

        <script src="{{ asset('plugins/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
        <script src="{{ asset('plugins/bootstrap-select/bootstrap-select.min.js') }}"></script>

        <!--Bootstrap Table Extension [ OPTIONAL ]-->
        <script src="{{ asset('plugins/bootstrap-table/extensions/editable/bootstrap-table-editable.js')}}"></script>

        <script src="{{ asset('plugins/masked-input/jquery.maskedinput.min.js')}}"></script>

        <script src="{{ asset('plugins/select2/js/select2.min.js') }}"></script>

        <script src="{{ asset('js/fintag.js') }}"></script>

        @stack('script')
    </body>
</html>
