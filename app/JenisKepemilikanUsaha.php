<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JenisKepemilikanUsaha extends Model
{
    protected $table = 'jenis_kepemilikan_usaha';

    protected $fillable = ['nama'];
}
