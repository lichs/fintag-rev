@extends('layouts.app')

@section('content')
  <div id="container" class="effect aside-float aside-bright mainnav-lg">

    <div id="bg-overlay" class="bg-img img-balloon1"></div>

    <div class="cls-content">
      <div class="cls-content-sm panel">
        <div class="panel-body">
          <div class="mar-ver pad-btm">
		        <h3 class="text-center">Login Agen Fintag</h3>
		        <p class="text-center">Akses menuju Akun Anda</p>
		      </div>
          <form method="post" action="{{ url('agent/login') }}">
            {{ csrf_field() }}
            <div class="form-group">
							<div class="input-group col-sm-12 col-xs-12">
								<input type="email" name="email" required="required" class="form-control" placeholder="Ketikkan Email Anda" autofocus>
							</div>
						</div>
						<div class="form-group">
							<div class="input-group  col-sm-12 col-xs-12">
								<input type="password" name="password" required="required" class="form-control" placeholder="Ketikkan Password Anda">
							</div>
						</div>
            <div class="checkbox pad-btm text-left">
		          <input id="demo-form-checkbox" class="magic-checkbox" type="checkbox">
		          <label for="demo-form-checkbox">Ingat saya</label>
		        </div>
            <div class="form-group">
		          <button class="btn btn-primary btn-block" type="submit">Masuk Akun</button>
						</div>
          </form>
          <div class="row ">
  					<div class="col-sm-12 mar-ver">
    					<div class="form-group">
    						<a href="{{ url('agent/register') }}"><button class="btn btn-block btn-dark">Silahkan Registrasikan diri Anda</button></a>
    					</div>
              <div class="text-center">
                <a href="#" class="btn-link">Lupa password ?</a>
              </div>
  					</div>
					</div>

        </div>
      </div>
    </div>

  </div>
@endsection
