@extends('layouts.app')

@section('content')
	<div id="container" class="effect aside-float aside-bright mainnav-lg">
		@foreach($agents as $agent)
			<div class="boxed">
				@include('agent.navigation')
					<div id="content-container">
					  <div id="page-content">
              <div class="panel">
              	<div class="panel-body pad-no">
                    <h3 class="panel-title">Form Pengajuan Badan Usaha</h3>
                    <ol class="breadcrumb">
                      <li class="home">Beranda</li>
                      <li><a href="#">Pendataan Pengajuan</a></li>
                      <li class="active">Form Pengajuan Badan Usaha</li>
                    </ol>
              	</div>
              </div>
              <div class="row">
                  <div class="eq-height">
                      <div class="col-sm-4 eq-box-sm">
                          <div class="panel">
                              <div class="panel-heading">
                                  <h3 class="panel-title">Profil Badan Usaha</h3>
                              </div>
                              <div class="panel-body">
                                  <p>Petunjuk keterangan mengenai profil badan usaha</p>
                                  <div class="demo-nifty-btn">
                                      <a href="{{ url('agent/pengajuan/profile-badan-usaha') }}">
                                          <button type="button" class="btn btn-default btn-icon">
                                              <i class="demo-psi-pen-5 icon-lg"></i>
                                          </button>
                                      </a>
                                  </div>
                              </div>
                              <div class="panel-footer">
                                  <div class="progress progress-sm">
                                    <div class="progress-bar progress-bar-primary progress-bar-striped active" aria-valuenow="15" aria-valuemin="0" aria-valuemax="100" style="width: 95%;" role="progressbar">
                                      <span class="sr-only">95%</span>
                                    </div>
                                  </div>
                                  <small>95% Completed</small>
                              </div>
                          </div>
                      </div>
                      <div class="col-sm-4 eq-box-sm">
                          <div class="panel">
                              <div class="panel-heading">
                                  <h3 class="panel-title">Profil Usaha</h3>
                              </div>
                              <div class="panel-body">
                                  <p>Petunjuk keterangan mengenai profil badan usaha</p>
                                  <div class="demo-nifty-btn">
                                      <a href="{{ url('agent/pengajuan/profile-usaha') }}">
                                          <button type="button" class="btn btn-default btn-icon">
                                              <i class="demo-psi-pen-5 icon-lg"></i>
                                          </button>
                                      </a>
                                  </div>
                              </div>
                              <div class="panel-footer">
                                  <div class="progress progress-sm">
                                    <div class="progress-bar progress-bar-warning progress-bar-striped active" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 50%;" role="progressbar">
                                      <span class="sr-only">50%</span>
                                    </div>
                                  </div>
                                  <small>50% Completed</small>
                              </div>
                          </div>
                      </div>
                      <div class="col-sm-4 eq-box-sm">
                          <div class="panel">
                              <div class="panel-heading">
                                  <h3 class="panel-title">Profil Keuangan</h3>
                              </div>
                              <div class="panel-body">
                                  <p>Petunjuk keterangan mengenai profil badan usaha</p>
                                  <div class="demo-nifty-btn">
                                      <a href="{{ url('agent/pengajuan/profile-keuangan') }}">
                                          <button type="button" class="btn btn-default btn-icon">
                                              <i class="demo-psi-pen-5 icon-lg"></i>
                                          </button>
                                      </a>
                                  </div>
                              </div>
                              <div class="panel-footer">
                                  <div class="progress progress-sm">
                                    <div class="progress-bar progress-bar-success progress-bar-striped active" aria-valuenow="15" aria-valuemin="0" aria-valuemax="100" style="width: 20%;" role="progressbar">
                                      <span class="sr-only">20%</span>
                                    </div>
                                  </div>
                                  <small>20% Completed</small>
                              </div>
                          </div>
                      </div>
                      <div class="col-sm-4 eq-box-sm">
                          <div class="panel">
                              <div class="panel-heading">
                                  <h3 class="panel-title">Profil Pembiayaan</h3>
                              </div>
                              <div class="panel-body">
                                  <p>Petunjuk keterangan mengenai profil badan usaha</p>
                                  <div class="demo-nifty-btn">
                                      <a href="{{ url('agent/pengajuan/profile-pembiayaan') }}">
                                          <button type="button" class="btn btn-default btn-icon">
                                              <i class="demo-psi-pen-5 icon-lg"></i>
                                          </button>
                                      </a>
                                  </div>
                              </div>
                              <div class="panel-footer">
                                  <div class="progress progress-sm">
                                    <div class="progress-bar progress-bar-danger progress-bar-striped active" aria-valuenow="15" aria-valuemin="0" aria-valuemax="100" style="width: 5%;" role="progressbar">
                                      <span class="sr-only">5%</span>
                                    </div>
                                  </div>
                                  <small>5% Completed</small>
                              </div>
                          </div>
                      </div>
                  </div>
                </div>
                <div class="panel">
                  <div class="panel-body">
                    <div class="col-sm-12 mar-top">
                      <div class="form-group">
                        <label class="col-md-3 control-label" >Tanggal Pengajuan *</label>
                        <div class="col-md-3">
                          <input type="text" id="demo-text-input" class="form-control dtpicker" >
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-12 mar-top">
                      <div class="form-group">
                        <label class="col-md-3 control-label" >Nama Pemohon*</label>
                        <div class="col-md-6">
                          <input type="text" id="demo-text-input" class="form-control" >
                        </div>
                      </div>
                    </div>

                    <div class="col-sm-12 mar-top">
                        <div class="checkbox">
                            <input id="persetujuan" class="magic-checkbox" type="checkbox">
                            <label for="persetujuan" class="pad=lft">Saya/ Kami adalah Warga Negara Indonesia, dengan ini menyatakan bahwa semua informasi tersebut yang disertai dengan dokumen penunjang yang berhubungan dengan keterangan-keterangan tersebut adalah BENAR dan SESUAI dengan aslinya, dan dengan ini memberikan kuasa kepada Perusahaan Keuangan untuk melakukan pengecekan terhadap pihak-pihak yang tercantum dalam Formulir Aplikasi tersebut.</label>
                        </div>
                    </div>
                    <div class="col-sm-12 mar-ver">
                    	  <button class="btn btn-primary" type="submit">Proses</button>
                    </div>
                  </div>
                </div>

					    </div>
					</div>
				@include('agent.sidebar')
			</div>
		@endforeach
	</div>
@endsection
