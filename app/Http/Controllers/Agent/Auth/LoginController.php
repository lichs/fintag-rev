<?php

namespace App\Http\Controllers\Agent\Auth;

use Auth;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{

    use AuthenticatesUsers;

    protected $redirectTo = '/agent';

    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    protected function authenticated( )
    {
      $id = Auth::id();

      $user = User::find($id);

      foreach ($user->role as $roles)
       {
          if ($roles->name === 'admin') {
            return redirect()->intended('/admin');
          } elseif ($roles->name === 'agent') {
            return redirect()->intended('/agent');
          } elseif ($roles->name === 'pu') {
            return redirect()->intended('/pelaku-usaha');
          }
       }

    }

    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->flush();

        $request->session()->regenerate();

        return redirect('/agent/login');
    }

    public function showLoginForm()
    {
        return view('agent.auth.login');
    }
}
