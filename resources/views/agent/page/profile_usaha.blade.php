@extends('layouts.app')

@section('content')
	<div id="container" class="effect aside-float aside-bright mainnav-lg">
		@foreach($agents as $agent)
			<div class="boxed">
				@include('agent.navigation')
					<div id="content-container">
					  <div id="page-content">
              <div class="panel">
              	<div class="panel-body pad-no">
                    <h3 class="panel-title">Form Pengajuan Perorangan</h3>
                    <ol class="breadcrumb">
                      <li class="home">Beranda</li>
                      <li><a href="#">Pendataan Pengajuan</a></li>
                      <li>Form Pengajuan Badan Usaha</li>
                      <li class="active">Profil Usaha</li>
                    </ol>
              	</div>
                <div class="panel-body">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam mauris eros, dictum id nisi convallis, efficitur ultrices mauris. In condimentum eu risus eu gravida. In hac habitasse platea dictumst. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Suspendisse finibus mollis magna, at porttitor felis dictum et. Nulla sit amet est velit. Donec malesuada urna vel nisl faucibus, sed laoreet urna porta. </p>
                </div>
              </div>
              <div class="row">
                  <div class="col-lg-12">
                      <div class="panel">
                          <div class="panel-heading">
                              <h3 class="panel-title">Profile Usaha</h3>
                          </div>
                          <form class="panel-body form-horizontal form-padding" action="/agent/pengajuan-perorangan/profile-pribadi" method="post">
															{{ csrf_field() }}
															<input type="hidden" name="cursor" value="2">
                              <div class="col-sm-10">
                                  <div class="pad-hor">
                                      <div class="form-group">
                                          <label class="col-md-3 control-label">Jenis Usaha yang Anda Jalani* </label>
                                          <div class="col-md-5">
                                            <select class="selectpicker form-control" onChange="setformJenisUsaha(this.value)">
                                            <option value="Nelayan Tangkap">Nelayan Tangkap</option>
                                            <option value="Nelayan Budidaya">Nelayan Budidaya</option>
                                            <option value="Pengolah">Pengolah</option>
                                            <option value="Pemasar">Pemasar</option>
                                            <option value="Supplier/Vendor">Supplier/ Vendor</option>
                                            <option value="Galangan Kapal">Galangan Kapal</option>
                                            </select>
                                          </div>
                                      </div>
                                      <div class="clearfix"></div>
                                      <div class="col-sm-10 field-group" id="formDataJenisUsaha1">
                                          <h3 class="h6 mar-ver">
                                              <span>Data Jenis Nelayan Tangkap / Budidaya</span>
                                          </h3>
                                          <div class="col-sm-10">
                                              <div class="form-group">
                                                  <label class="col-md-4 control-label">Nomor Kartu Nelayan *</label>
                                                  <div class="col-md-5">
                                                      <input type="text" name="" class="form-control">
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="col-sm-10">
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label" >Status Kepemilikan Kapal *</label>
                                                    <div class="col-md-8">
                                                        <div class="radio">
                                                            <input id="rbtStatusKap-1" class="magic-radio" type="radio" name="rbtStatusKap" value="Milik Sendiri"  checked>
                                                            <label for="rbtStatusKap-1">Milik Sendiri</label>
                                                            <input id="rbtStatusKap-2" class="magic-radio" type="radio" name="rbtStatusKap" value="Pinjam">
                                                            <label for="rbtStatusKap-2">Pinjam</label>
                                                            <input id="rbtStatusKap-3" class="magic-radio" type="radio" name="rbtStatusKap" value="Tidak Punya">
                                                            <label for="rbtStatusKap-3">Tidak Punya</label>
                                                        </div>
                                                    </div>
                                                </div>
                                          </div>
                                          <div class="col-sm-10">
                                              <div class="form-group">
                                                  <label class="col-md-4 control-label" >Ukuran Kapal *</label>
                                                  <div class="col-md-8">
                                                    <div class="col-md-3">
                                                        <input type="number" min="0"  class="form-control" >
                                                    </div>
                                                    <div class="col-md-5">
                                                        gross ton</div>
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="mar-ver"></div>
                                    </div>
																		<div class="clearfix"></div>
																		<div class="col-sm-10 field-group" id="formDataJenisUsaha2">
																					<h3 class="h6 mar-ver"><span>Data Pengolah/ Pemasar/ Supplier/ Galangan Kapal</span></h3>
																					<div class="col-sm-10">
																							<div class="form-group">
																									<label class="col-md-4 control-label" >Surat Izin Usaha *</label>
																									<div class="col-md-5">
																											<input type="text"  class="form-control" >
																									</div>
																							</div>
																					</div>
																					<div class="mar-ver"></div>
																		</div>
                                    <div class="col-sm-10">
                                        <h3 class="h5 mar-ver"><span>Usaha Lain</span></h3>
                                        <div class="pad-hor">
                                            <div class="form-group">
                                                <label class="col-md-3 control-label" >Apakah sedang bekerja di tempat lain *</label>
                                                <div class="col-md-9">
                                                    <div class="radio">
                                                    <input id="rbtUsaha-1" class="magic-radio" type="radio" name="rbtUsaha" onClick="setformPekerjaanLain(this.value)" value="Ya">
                                                    <label for="rbtUsaha-1">Ya</label>
                                                    <input id="rbtUsaha-2" class="magic-radio" type="radio" name="rbtUsaha" onClick="setformPekerjaanLain(this.value)" value="Tidak"   checked >
                                                    <label for="rbtUsaha-2">Tidak</label>
                                                    </div>
                                                </div>
                                            </div>
																						<div class="col-sm-10 field-group" id="formDataPekerjaanLain">
																								<h3 class="h6 mar-ver"><span>Informasi Pekerjaan Lain</span></h3>
																								<div class="col-sm-10">
																										<div class="form-group">
																												<label class="col-md-4 control-label" >Nama Perusahaan *</label>
																												<div class="col-md-6">
																														<input type="text"  class="form-control" >
																												</div>
																										</div>
																								</div>
																								<div class="col-sm-10">
																										<div class="form-group">
																												<label class="col-md-4 control-label" >No Telp Kantor *</label>
																												<div class="col-md-6">
																														<input type="text"  class="form-control" >
																												</div>
																										</div>
																								</div>
																								<div class="col-sm-10">
																										<div class="form-group">
																												<label class="col-md-4 control-label" >Jabatan *</label>
																												<div class="col-md-6">
																														<input type="text"  class="form-control" >
																												</div>
																										</div>
																								</div>
																								<div class="col-sm-10">
																										<div class="form-group">
																												<label class="col-md-4 control-label" >Bekerja Mulai Tahun *</label>
																												<div class="col-md-2">
																														<input type="number" min="0"  class="form-control">
																												</div>
																										</div>
																								</div>
																								<div class="mar-ver"></div>
																						</div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 mar-ver">
                                      <div class="col-sm-4">
                                      	<a class="btn btn-default btn-block mar-ver" type="button" href="{{ redirect()->back()->getTargetUrl() }}">Tutup Halaman ini</a>
                                      </div>
                                      <div class="col-sm-4">
                                      	<button class="btn btn-primary btn-block mar-ver" type="submit">Simpan</button>
                                      </div>
                                    </div>
                              </div>
                          </form>
                      </div>
                  </div>
              </div>
            </div>
          </div>
				@include('agent.sidebar')
			</div>
		@endforeach
	</div>
@endsection

@push('script')
	<script>
	$( document ).ready(function() {
			$('.dtpicker').datepicker({
			dateFormat : "dd/mm/yy"
			});
			$('.selectpicker').selectpicker('refresh');
			$('#formDataPekerjaanLain').hide();
			$('#formDataJenisUsaha2').hide();
			});

			function setformJenisUsaha(val) {
			if (val=='Nelayan Tangkap' || val=='Nelayan Budidaya') {
				$('#formDataJenisUsaha1').show();
				$('#formDataJenisUsaha2').hide();
			}else if (val=='Pengolah' || val=='Pemasar' || val=='Supplier/Vendor' || val=='Galangan Kapal') {
				$('#formDataJenisUsaha2').show();
				$('#formDataJenisUsaha1').hide();
			}
			}
			function setformPekerjaanLain(val) {
			if (val=='Ya') {
			$('#formDataPekerjaanLain').show();
			}else{
			$('#formDataPekerjaanLain').hide();
			}
	}
	</script>
@endpush
