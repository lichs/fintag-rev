@extends('layouts.app')

@section('content')

	<div id="container" class="effect aside-float aside-bright mainnav-lg">

		@foreach($agents as $agent)
			<div class="boxed">
				@include('agent.navigation')
          @if ( Session::get('message') === "created" )
              @push('script')
                  <script>
                  $.niftyNoty({
                          type: 'info',
                          title: 'Pesan.',
                          message: 'Request anda telah dibuat<br> Silahkan confirm request anda untuk melanjutkan proses',
                          container: 'floating',
                          timer: 5000
                        });
                  </script>
              @endpush
          @elseif ( Session::get('message') === "updated" )
						@push('script')
								<script>
								$.niftyNoty({
												type: 'success',
												title: 'Pesan.',
												message: 'Permintaan Pengajuan anda sudah anda terima, silahkan menuju menu Data Pengajuan untuk proses selanjutnya',
												container: 'floating',
												timer: 5000
											});
								</script>
						@endpush
					@elseif ( Session::get('message') === "rejected" )
						@push('script')
								<script>
								$.niftyNoty({
												type: 'danger',
												title: 'Pesan.',
												message: 'Permintaan Pengajuan anda sudah anda tolak, untuk melihat semua permintaan pengajuan silahkan menuju tab <strong>History</strong>',
												container: 'floating',
												timer: 5000
											});
								</script>
						@endpush
					@endif
					<div id="content-container">
						<div id="page-content">
							<div class="panel">
								<div class="panel-body pad-no">
									<div class="col-sm-12 col-lg-6">
										<h3 class="panel-title">Permintaan Pengajuan</h3>
										<ol class="breadcrumb">
											<li class="home"><a href="{{ url('/agent') }}">Beranda</a></li>
											<li class="active">Permintaan Pengajuan</li>
										</ol>
									</div>
									<div class="panel-body pad-hor">
										<div class="col-sm-12 col-lg-6">
											<div class="pull-right">
												<button class="btn btn-primary btn-lg btn-icon btn-fill" data-target="#demo-default-modal" data-toggle="modal"><i class="fa fa-plus"></i> Minta Pengajuan</button>
											</div>
										</div>
									</div>
								</div>
								<div class="panel-body pad-hor">
									<div class="tab-base">
										<ul class="nav nav-tabs pad-hor">
											<li class="active">
												<a data-toggle="tab" href="#demo-lft-tab-1">Precheck <span class="badge badge-purple">{{ $countRequest }}</span></a>
											</li>
											<li>
												<a data-toggle="tab" href="#demo-lft-tab-2">History</a>
											</li>
										</ul>
										<div class="tab-content">
											<div id="demo-lft-tab-1" class="tab-pane fade active in">
												<div class="fluid">
													<div class="panel">
														<div class="panel-body">
															<fieldset>
																<div class="form-group">
																	<div class="col-sm-3 mar-ver">
																		<div class="input-group date">
																			<input type="text" placeholder="Pilih Tanggal Awal"  class="form-control dtpicker" id="test">
																			<span class="input-group-addon">
																				<i class="pli-calendar-4"></i>
																			</span>
													 					</div>
																	</div>
																	<div class="col-sm-3 mar-ver col-sm-offset-1">
																		<div class="input-group date">
																			<input type="text" placeholder="Pilih Tanggal Akhir"  class="form-control dtpicker">
																			<span class="input-group-addon">
																				<i class="pli-calendar-4"></i>
																			</span>
																		</div>
																	</div>
																	<div class="col-sm-1 mar-ver">
																		<button class="btn btn-default btn-icon" type="submit">
																			<i class="fa fa-search"></i>
																		</button>
																	</div>
																</div>
															</fieldset>

															<div class="clearfix mar-ver"></div>

															<table id="request-agent" class="table table-striped table-bordered" cellspacing="0" width="100%">
																<thead>
																	<tr>
																		<th>Ticket no.</th>
																		<th>Pelaku Usaha</th>
																		<th>No Telp</th>
																		<th>Tanggal Request</th>
																		<th>Action</th>
																		<th>History Pengajuan</th>
																	</tr>
																</thead>
															</table>

														</div>
													</div>
												</div>
											</div>
											<div id="demo-lft-tab-2" class="tab-pane fade">
												<div class="fluid">
													<div class="panel">
														<div class="panel-body">
															<fieldset>
																<div class="form-group">
																	<div class="col-sm-3 mar-ver">
																		<div class="input-group date">
																			<input type="text" placeholder="Pilih Tanggal Awal"  class="form-control dtpicker" id="test">
																			<span class="input-group-addon">
																				<i class="pli-calendar-4"></i>
																			</span>
													 					</div>
																	</div>
																	<div class="col-sm-3 mar-ver col-sm-offset-1">
																		<div class="input-group date">
																			<input type="text" placeholder="Pilih Tanggal Akhir"  class="form-control dtpicker">
																			<span class="input-group-addon">
																				<i class="pli-calendar-4"></i>
																			</span>
																		</div>
																	</div>
																	<div class="col-sm-1 mar-ver">
																		<button class="btn btn-default btn-icon" type="submit">
																			<i class="fa fa-search"></i>
																		</button>
																	</div>
																</div>
															</fieldset>

															<div class="clearfix mar-ver"></div>

															<table id="history-request" class="table table-striped table-bordered" cellspacing="0" width="100%">
																<thead>
																	<tr>
																		<th>Ticket no.</th>
																		<th>Pelaku Usaha</th>
																		<th>No Telp</th>
																		<th>Tanggal Request</th>
																		<th>Status</th>
																	</tr>
																</thead>
															</table>

														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				@include('agent.sidebar')
			</div>


		<div class="modal fade" id="demo-default-modal" role="dialog" tabindex="-1" aria-labelledby="demo-default-modal" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" title="Tutup Form ini" class="close" data-dismiss="modal"><i class="fa fa-remove"></i></button>
						<h4 class="modal-title text-center">Tambah Permintaan Pengajuan</h4>
					</div>
					<form action="{{ url('agent/permintaan-pengajuan') }}" class="form-horizontal" method="post">
						{{ csrf_field() }}
            <input type="hidden" name="agent_id" value="" id="agent_id">
						<div class="modal-body">
							<div class="col-sm-12">
								<h3 class="h5 mar-ver"><span>Data Pelaku Usaha</span></h3>
							</div>
							<div class="form-group fix">
								<label class="col-lg-3 control-label">NIK</label>
								<div class="col-lg-7 text-left">
									<div id="xnik"><input type="text" class="form-control" name="nik" id="nik"  placeholder="NIK" required /></div>
									<span id="result"></span>
								</div>
								<button type="button" class="btn btn-sm btn-info" id="cek">Cek</button>
							</div>
							<div id="inputPU">
	                <div class="form-group">
	                  <label class="col-lg-3 control-label">Nama Lengkap</label>
	                  <div class="col-lg-7 text-left">
	                    <input type="text" class="form-control" name="nama" id="nama" placeholder="Nama Lengkap" required/>
	                  </div>
	                  <span id="result" class="text-small text-danger"></span>
	                </div>
									<div class="form-group">
										<label class="col-lg-3 control-label">Nomer telepon</label>
										<div class="col-lg-7 text-left">
											<input type="text" class="form-control" name="no_telp" id="no_telp" placeholder="Nomer Telepon" required/>
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-3 control-label">Email</label>
										<div class="col-lg-7 text-left">
											<input type="email" class="form-control" name="email" id="email" placeholder="Email" required/>
										</div>
										<span id="result" class="text-small text-danger"></span>
									</div>
									<div class="form-group">
										<label class="col-lg-3 control-label">Alamat</label>
										<div class="col-lg-7 text-left">
											<input type="text" class="form-control" name="alamat" id="alamat" placeholder="Alamat" required/>
										</div>
									</div>
									<input type="hidden" name="email_agen"/>
									<div class="clearfix"></div>
							</div>
							<div class="modal-footer">
								<button id="tambah" class="btn btn-primary btn-fill" name="sub_add_pu" type="submit">Simpan</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>

		@endforeach

	</div>

  <script id="details-template" type="text/x-handlebars-template">
        <table class="table details-table" id="@{{ id }}">
            <thead>
            <tr>
                <th>Kode Pengajuan</th>
                <th>Tanggal Pengajuan</th>
                <th>Tanggal Selesai Pengajuan</th>
                <th>Tanggal Kelengkapan Data</th>
                <th>Status</th>
            </tr>
            </thead>
        </table>
    </script>

@endsection

@push('style')
		<style>

				.details-control{
						cursor: pointer;
				}

		</style>
@endpush

@push('script')
    <script>

		$('#xnik').keypress(function(e){
				if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57 )) {
						$('#result').html("Hanya angka untuk kolom NIK").show();
						return false;
				} else {
						$('#result').hide();
				}
		});

    $('#inputPU').hide();
    $('.modal-footer').hide();

		$('#history-request').DataTable({
			responsive: true,
			processing: true,
			serverSide: true,
			ajax: "/agent/data/history-request",
			columns: [
					{ data: 'ticket_no', name: 'Ticket No.' , orderable: true},
					{ data: 'pelaku_usaha.nama', name: 'Pelaku Usaha'},
					{ data: 'pelaku_usaha.no_telp', name: 'No Telp'},
					{ data: 'created_at', name: 'Tanggal Request' },
					{ data: 'status', name: 'Status', orderable: false, searchable: false}
				]
		});

    var template = Handlebars.compile($("#details-template").html());
    var table = $('#request-agent').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        ajax: "/agent/data/request-agent",
        columns: [
          { data: 'ticket_no', name: 'Ticket No.' , orderable: true},
          { data: 'pelaku_usaha.nama', name: 'Pelaku Usaha'},
          { data: 'pelaku_usaha.no_telp', name: 'No Telp'},
          { data: 'created_at', name: 'Tanggal Request' },
          { data: 'action', name: 'Action', orderable: false, searchable: false},
          {
                    "className":      'details-control',
                    "orderable":      false,
                    "data":           null,
                    "defaultContent": '<button class="btn btn-sm btn-mint">Check</button>'
          }
        ]
    });

    $('#request-agent tbody').on('click', 'td.details-control', function () {
	        var tr = $(this).closest('tr');
	        var row = table.row( tr );
	        var tableId = row.data().id;
	 		    var pelaku_usaha_id = row.data().pelaku_usaha_id;

	        console.log(pelaku_usaha_id);

	        if ( row.child.isShown() ) {
	            // This row is already open - close it
	            row.child.hide();
	            tr.removeClass('shown');
	        }
	        else {
	            // Open this row
	            row.child(template(row.data())).show();
	            initTable(tableId, row.data(), pelaku_usaha_id);
	            tr.addClass('shown');
	            tr.next().find('td').addClass('no-padding');
	        }
	    } );

  	function initTable(tableId, data, pelaku_usaha_id) {
          $('#' + tableId ).DataTable({
              processing: true,
              serverSide: true,
              ajax:  "/agent/data/detail-request/" + pelaku_usaha_id,
              columns: [
                  { data: 'kode', name: 'Kode Pengajuan' },
                  { data: 'created_at', name: 'Tanggal Pengajuan' },
                  { data: 'tanggal_selesai', name: 'Tanggal Selesai Pengajuan'},
                  { data: 'tanggal_kelengkapan_data', name: 'Tanggal Kelengkapan Data'},
                  { data: 'status', name: 'Status'}
              ]
          })
      };

      $('#cek').click(function(e){
          e.preventDefault();

          $.get("/agent/data/cek-nik/" + $('#nik').val(), function(data){

            if (data.length == 0) {
              $('#inputPU').slideDown();
              $('.modal-footer').show();
            } else {
              $('#inputPU').slideDown();
              $('.modal-footer').show();

              $.each(data, function(key, value){
                  $('#agent_id').val(value.id);
                  $('#email').val(value.email);
                  $('#nama').val(value.nama);
        					$('#alamat').val(value.alamat);
        					$('#no_telp').val(value.no_telp);
              });
            }

          });
      });
    </script>
@endpush
