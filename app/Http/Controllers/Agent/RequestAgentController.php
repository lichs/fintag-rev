<?php

namespace App\Http\Controllers\Agent;

use App\RequestAgent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RequestAgentController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:agent');
    }

    public function terimaPermintaanPengajuan($id)
    {
        $request = RequestAgent::find($id);
        $request->accepted = 1;
        $request->save();
        return redirect('/agent/permintaan-pengajuan')->with('message', 'updated');
    }

    public function tolakPermintaanPengajuan($id)
    {
        $request = RequestAgent::find($id);
        $request->accepted = 2;
        $request->save();
        return redirect('/agent/permintaan-pengajuan')->with('message', 'rejected');
    }
}
