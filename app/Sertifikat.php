<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sertifikat extends Model
{
    protected $table = 'sertifikat';

    protected $fillable = ['agent_id', 'no_sertifikat', 'tanggal_awal', 'tanggal_akhir', 'sertifikat_image'];

    public function agent()
    {
      return $this->belongsToMany(Agent::class, 'agent_id');
    }
}
