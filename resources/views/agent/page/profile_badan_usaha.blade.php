@extends('layouts.app')

@section('content')
	<div id="container" class="effect aside-float aside-bright mainnav-lg">
		@foreach($agents as $agent)
			<div class="boxed">
				@include('agent.navigation')
					<div id="content-container">
					  <div id="page-content">
              <div class="panel">
              	<div class="panel-body pad-no">
                    <h3 class="panel-title">Form Pengajuan Badan Usaha</h3>
                    <ol class="breadcrumb">
                      <li class="home">Beranda</li>
                      <li><a href="#">Pendataan Pengajuan</a></li>
                      <li>Form Pengajuan Badan Usaha</li>
                      <li class="active">Profil Badan Usaha</li>
                    </ol>
              	</div>
                <div class="panel-body">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam mauris eros, dictum id nisi convallis, efficitur ultrices mauris. In condimentum eu risus eu gravida. In hac habitasse platea dictumst. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Suspendisse finibus mollis magna, at porttitor felis dictum et. Nulla sit amet est velit. Donec malesuada urna vel nisl faucibus, sed laoreet urna porta. </p>
                </div>
              </div>
              <div class="row">
                  <div class="col-lg-12">
                      <div class="panel">
                          <div class="panel-heading">
                              <h3 class="panel-title">Profile Badan Usaha</h3>
                          </div>
                          <form class="panel-body form-horizontal form-padding" action="#" method="post">
                              <div class="col-sm-12">
                                  <h3 class="h5 mar-ver">
                                      <span>Identitas Badan Usaha</span>
                                  </h3>
                                  <div class="pad-hor">
                                      <div class="col-sm-10">
                                        <div class="form-group">
                                          <label class="col-md-3 control-label">Nama *</label>
                                          <div class="col-md-9">
                                            <input type="text" name="nama" class="form-control">
                                          </div>
                                        </div>
                                      </div>
                                      <div class="col-sm-10">
                                        <div class="form-group">
                                          <label class="col-md-3 control-label">Kepemilikan Usaha *</label>
                                          <div class="col-md-4">
                                            <select class="selectpicker form-control" id="showAnggotaKoperasi">
                                                <option value="Kelompok Usaha Bersama">Kelompok Usaha Bersama</option>
                                                <option value="Koperasi">Koperasi</option>
                                                <option value="CV">CV</option>
                                                <option value="PT">PT</option>
                                                <option value="UD">UD</option>
                                            </select>
                                          </div>
																					<div id="formAnggotaKoperasi">
																							<div class="clearfix mar-top"></div>
																							<div class="clearfix mar-top"></div>
																							<div class="col-sm-10 field-group" >
																									<h3 class="h6 mar-ver"><span>Data Jumlah Anggota Koperasi</span></h3>
																									<div class="col-md-10" >
																										<div class="form-group">
																												<label class="col-md-5 control-label" >Jumlah Anggota Koperasi *</label>
																												<div class="col-md-3">
																														<input type="number" min="0"  class="form-control" >
																												</div>
																										</div>
																									</div>
																							</div>
																					</div>
                                        </div>
                                      </div>
                                      <div class="col-sm-10">
                                        <div class="form-group">
                                          <label class="col-md-3 control-label">Alamat *</label>
                                          <div class="col-md-9">
                                            <input type="text" name="alamat" class="form-control">
                                          </div>
                                        </div>
                                      </div>
                                      <div class="col-sm-10">
                                        <div class="form-group">
                                          <label class="col-md-3 control-label">Telepon Kantor *</label>
                                          <div class="col-md-9">
                                            <input type="text" name="telp" class="form-control">
                                          </div>
                                        </div>
                                      </div>
                                      <div class="col-sm-10">
                                        <div class="form-group">
                                          <label class="col-md-3 control-label">Fax Kantor *</label>
                                          <div class="col-md-9">
                                            <input type="text" name="fax" class="form-control">
                                          </div>
                                        </div>
                                      </div>
                                      <div class="col-sm-10">
                                        <div class="form-group">
                                          <label class="col-md-3 control-label">Contact Person *</label>
                                          <div class="col-md-9">
                                            <input type="text" name="cp" class="form-control">
                                          </div>
                                        </div>
                                      </div>
                                      <div class="col-sm-10">
                                        <div class="form-group">
                                          <label class="col-md-3 control-label">Alamat Email *</label>
                                          <div class="col-md-9">
                                            <input type="text" name="email" class="form-control">
                                          </div>
                                        </div>
                                      </div>
                                      <div class="col-sm-10">
                                        <div class="form-group">
                                          <label class="col-md-3 control-label">Mulai Usaha Tahun *</label>
                                          <div class="col-md-9">
                                            <input type="text" name="mulai_usaha" class="form-control">
                                          </div>
                                        </div>
                                      </div>
                                      <div class="col-sm-10">
                                        <div class="form-group">
                                          <label class="col-md-3 control-label">Bidang Usaha *</label>
                                          <div class="col-md-9">
                                            <input type="text" name="bidang_usaha" class="form-control">
                                          </div>
                                        </div>
                                      </div>
                                  </div>
                              </div>

                              <div class="col-sm-12">
                                <h3 class="h5 mar-ver"><span>Izin Usaha</span></h3>
                                <div class="pad-hor">
                                    <div class="col-sm-10">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" >Nomor SIUP *</label>
                                            <div class="col-md-9">
                                                <input type="text"  class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-10">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" >Nomor TDP *</label>
                                            <div class="col-md-9">
                                                <input type="text"  class="form-control" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-10">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" >Nomor NPWP *</label>
                                            <div class="col-md-4">
                                                <input type="text"  class="form-control text-left" id="npwp" onKeyPress="return numbersonly(this, event)" >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                              </div>

                              <div class="col-sm-12">
                                  <h3 class="h5 mar-ver"><span>Kepengurusan</span></h3>
                                  <div class="pad-hor">
                                      <div class="col-sm-4 mar-ver">
                                      	<button class="btn btn-primary btn-hover-mint"><i class="fa fa-plus-circle"></i> Tambah Pengurus</button>
                                      </div>
                                      <div class="col-sm-10">
                                          <div class="form-group">
                                              <label class="col-md-3 control-label" >Nama Pengurus *</label>
                                              <div class="col-md-9">
                                                  <input type="text"  class="form-control" >
                                              </div>
                                          </div>
                                      </div>
                                      <div class="col-sm-10">
                                          <div class="form-group">
                                              <label class="col-md-3 control-label" >Jabatan *</label>
                                              <div class="col-md-4">
                                                  <select class="selectpicker form-control">
                                                  <option value="Pemilik">Pemilik</option>
                                                  <option value="Direktur/Dirut">Direktur/Dirut</option>
                                                  <option value="Komisaris/Komut">Komisaris/Komut</option>
                                                  </select>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="col-sm-10">
                                          <div class="form-group">
                                              <label class="col-md-3 control-label" >Saham *</label>
                                              <div class="col-md-9">
                                                  <input type="text"  class="form-control" >
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                              </div>

                              <div class="col-sm-12">
                                  <h3 class="h5 mar-ver"><span>Rekening Bank Aktif</span></h3>
                                  <div class="pad-hor">
                                      <div class="col-sm-4 mar-ver">
                                      	<button class="btn btn-primary btn-hover-mint"><i class="fa fa-plus-circle"></i> Tambah Rekening</button>
                                      </div>
                                      <div class="col-sm-10">
                                          <div class="form-group">
                                          <label class="col-md-3 control-label" >Nama Bank *</label>
                                          <div class="col-md-9">
                                          <input type="text"  class="form-control" >
                                          </div>
                                          </div>
                                      </div>
                                      <div class="col-sm-10">
                                          <div class="form-group">
                                            <label class="col-md-3 control-label" >Cabang *</label>
                                            <div class="col-md-9">
                                                <input type="text"  class="form-control" >
                                            </div>
                                          </div>
                                      </div>
                                      <div class="col-sm-10">
                                          <div class="form-group">
                                              <label class="col-md-3 control-label" >Nomor Rekening *</label>
                                              <div class="col-md-9">
                                                  <input type="text"  class="form-control" onKeyPress="return numbersonly(this, event)" >
                                              </div>
                                          </div>
                                      </div>
                                      <div class="col-sm-10">
                                          <div class="form-group">
                                              <label class="col-md-3 control-label" >Atas Nama *</label>
                                              <div class="col-md-9">
                                                  <input type="text"  class="form-control" >
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                              </div>

                              <div class="col-sm-12 mar-ver">
                                  <div class="col-sm-4">
                                	  <button class="btn btn-default btn-block mar-ver" type="button">Tutup Halaman ini</button>
                                  </div>
                                  <div class="col-sm-4">
                                	   <button class="btn btn-primary btn-block mar-ver" type="submit">Simpan</button>
                                  </div>
                              </div>

                          </form>
                      </div>
                  </div>
              </div>
            </div>
					</div>
				@include('agent.sidebar')
			</div>
		@endforeach
	</div>
@endsection

@push('script')
		<script>
		$(document).ready(function(){

			$('#formAnggotaKoperasi').hide();

			$('#showAnggotaKoperasi').change(function(){
				if ($(this).val()=='Koperasi') {
						$('#formAnggotaKoperasi').show();
						}else{
						$('#formAnggotaKoperasi').hide();
				}
			})
		})

		</script>
@endpush
