<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePengajuanPusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('pengajuan_pu', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('request_id')->unsigned();
          $table->integer('agent_id')->unsigned();
          $table->integer('pelaku_usaha_id')->unsigned();
          $table->integer('jenis_usaha_id')->unsigned();
          $table->integer('jenis_pengajuan_id')->unsigned();
          $table->integer('jenis_kepemilikan_usaha_id')->unsigned();
          $table->timestamp('tanggal_pengajuan')->nullable();
          $table->timestamp('tanggal_selesai')->nullable();
          $table->timestamp('tanggal_kelengkapan_data')->nullable();
          $table->time('jam_awal_pengajuan')->nullable();
          $table->string('kode')->nullable();
          $table->smallInteger('status')->nullable();
          $table->string('file_proposal')->nullable();
          $table->string('keterangan_proposal')->nullable();
          $table->string('keterangan_pengajuan')->nullable();
          $table->smallInteger('status_respon_pengajuan')->nullable();
          $table->smallInteger('status_respon_kelengkapan_data')->nullable();
          $table->smallInteger('status_respon_pilih_pp')->nullable();
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengajuan_pu');
    }
}
