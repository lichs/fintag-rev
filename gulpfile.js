const elixir = require('laravel-elixir');

elixir(mix => {
    mix.scripts(['profile.js'], 'public/js/fintag.js');
});
