<header id="navbar" style="border: 0px;">
	<div id="navbar-container" class="boxed">
		<div class="navbar-header">
            <a href="#" class="navbar-brand">
                <img src="{{ asset('img/logo.png') }}" alt="AGEN FINTAG" class="brand-icon">
                <div class="brand-title">
                    <span class="brand-text">AGEN FINTAG</span>
                </div>
            </a>
        </div>
        <div class="navbar-content clearfix">
        	<ul class="nav navbar-top-links pull-left">
        		<li class="tgl-menu-btn">
                    <a class="mainnav-toggle" href="#">
                        <i class="fa fa-reorder"></i>
                    </a>
                </li>

				<!--Notification dropdown-->
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <li class="dropdown">
                    <a href="#" data-toggle="dropdown" class="dropdown-toggle">
                        <i class="fa fa-bell-o"></i>
                        <span class="badge badge-header badge-danger">9</span>
                    </a>

                	<!--Notification dropdown menu-->
                    <div class="dropdown-menu dropdown-menu-sm">
                        <div class="pad-all bord-btm">
                            <p class="text-semibold text-main mar-no">Ada 9 notifikasi yang belum Anda lihat</p>
                        </div>

                        <!--Dropdown footer-->
                        <div class="pad-all bord-top">
                            <a href="#" class="btn-link text-dark box-block">
                                Lihat semua notifikasi
                            </a>
                        </div>
                    </div>
                </li>
               	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <!--End notifications dropdown-->
        	</ul>


                <ul class="nav navbar-top-links pull-right pad-rgt">
                    <li id="dropdown-user" class="dropdown">
                        <a href="#" data-toggle="dropdown" class="dropdown-toggle text-right">
                            <span class="pull-right">
                                <img class="img-square img-user media-object"
                                src="{{ Storage::url('public/agent_profile/' . $agent->agent_image) }}" alt="Profile Picture">
                            </span>
                            <div class="username hidden-xs"><strong>{{ $agent->nama }}</strong></div>
                        </a>
                    </li>
                </ul>

        </div>
	</div>
</header>
