@extends('layouts.app')

@section('content')

	<div id="container" class="effect aside-float aside-bright mainnav-lg">
		<div id="bg-overlay" class="bg-img img-balloon1"></div>

			<div class="cls-content mar-btm">
				<div class="cls-content-lg panel">
					<div class="panel-heading mar-ver">
						<h3 class="panel-title text-danger text-center text-2x pad-top">Verifikasi Akun Agen</h3>
		        <p class="text-no text-sm text-center mar-btm">Setelah melakukan verifikasi maka anda dapat menggunakan aplikasi sepenuhnya</p>
					</div>

					<div class="panel-body">
						<form action="/agent/isi-profile" method="post" enctype="multipart/form-data" id="verifikasiProfile">
		            {{ csrf_field() }}

		            <div class="row">
		              <h3 class="h5 mar-ver" style="padding-left: 10px;"><span>Data Pribadi</span></h3>
		              <div class="col-sm-12">
  									<div class="form-group{{ $errors->has('nama') ? ' has-error': '' }}">
  										<input type="text" class="form-control" placeholder="Nama Lengkap" name="nama" value="{{ old('nama') }}">
  									</div>
  									@if($errors->has('nama'))
  										<small class="help-block">
  											<strong>{{ $errors->first('nama') }}</strong>
  										</small>
  									@endif
								  </div>
		            </div>

  							<div class="row">
  								<div class="col-sm-6">
  									<div class="form-group{{ $errors->has('tmptLahir') ? ' has-error' : '' }}">
  										<input type="text" class="form-control" placeholder="Tempat Lahir" name="tmptLahir" value="{{ old('tmptLahir') }}">
  										@if($errors->has('tmptLahir'))
  											<small class="help-block">
  												<strong>{{ $errors->first('tmptLahir') }}</strong>
  											</small>
  										@endif
  							 		</div>
  								</div>
  								<div class="col-sm-6">
  									<div class="form-group{{ $errors->has('tglLahir') ? ' has-error' : '' }}">
                      <div class="input-group col-sm-12 col-xs-12">
                        <input type="date" class="form-control" name="tglLahir" placeholder="Tanggal Lahir" value="{{ old('tglLahir') }}" style="height: 31px">
                      </div>
  										@if($errors->has('tglLahir'))
  								        <small class="help-block">
  									        <strong>{{ $errors->first('tglLahir') }}</strong>
  										    </small>
  										@endif
  									</div>
  								</div>
  							</div>

  							<div class="row">
  								<div class="col-sm-6">
  									<div class="form-inline{{ $errors->has('jns_kelamin') ? ' has-error' : '' }}" style="text-align: left; margin-top: 10px;">
  										<div class="checkbox mar-rgt">
  											<label>
  												<input type="radio" value="pria" name="jns_kelamin" value="pria" id="demo-form-radiobox" class="magic-radiobox">
  												Pria
  											</label>
  										</div>
  										<div class="checkbox">
  											<label>
  												<input type="radio" value="wanita" name="jns_kelamin" value="wanita"> Wanita
  											</label>
  										</div>
  										@if($errors->has('jns_kelamin'))
  											<small class="help-block">
  												<strong>{{ $errors->first('jns_kelamin') }}</strong>
  											</small>
  										@endif
  									</div>
  								</div>
  								<div class="col-sm-6">
  									<div class="form-group{{ $errors->has('telp') ? ' has-error' : '' }}">
  										<input type="text" class="form-control" name="telp" placeholder="No Telp" value="{{ old('telp') }}" id="no_telp">
  									</div>
  									@if($errors->has('telp'))
  										<small class="help-block">
  											<strong>{{ $errors->first('telp') }}</strong>
  										</small>
  									@endif
  								</div>
  							</div>

  							<div class="row">
  								<div class="col-sm-6">
  									<div class="form-group{{ $errors->has('provinsi') ? ' has-error' : '' }}">
  										<select name="provinsi" id="provinsi" class="form-control select2">
  											@foreach($provinsis as $provinsi)
  												<option value="{{ $provinsi->id }}">{{$provinsi->name}}</option>
  											@endforeach
  										</select>
  									</div>
  									@if($errors->has('provinsi'))
  										<small class="help-block">
  											<strong>{{ $errors->first('provinsi') }}</strong>
  										</small>
  									@endif
  								</div>
  								<div class="col-sm-6">
  									<div class="form-group{{ $errors->has('kota') ? ' has-error' : '' }}">
  										<select name="kota" id="kabupaten" class="form-control select2">
  											<option value="">Pilih Kabupaten / Kota</option>
  										</select>
  									</div>
  									@if($errors->has('kota'))
  										<small class="help-block">
  											<strong>{{ $errors->first('kota') }}</strong>
  										</small>
  									@endif
  								</div>
  								<div class="col-sm-6">
  									<div class="form-group{{ $errors->has('kelurahan') ? ' has-error' : '' }}">
  										<select name="kecamatan" id="kecamatan" class="form-control select2">
  											<option value="">Pilih Kecamatan</option>
  										</select>
  									</div>
  									@if($errors->has('kelurahan'))
  										<small class="help-block">
  											<strong>{{ $errors->first('kelurahan') }}</strong>
  										</small>
  									@endif
  								</div>
  								<div class="col-sm-6">
  									<div class="form-group{{ $errors->has('kelurahan') ? ' has-error' : '' }}">
  										<select name="kelurahan" id="kelurahan" class="form-control select2">
  											<option value="">Pilih Kelurahan</option>
  										</select>
  									</div>
  									@if($errors->has('kecamatan'))
  										<small class="help-block">
  											<strong>{{ $errors->first('kecamatan') }}</strong>
  										</small>
  									@endif
  								</div>
  							</div>

  							<div class="row">
  		            <div class="col-sm-6">
  									<div class="form-group{{ $errors->has('alamat') ? ' has-error' : '' }}">
  										<input type="text" class="form-control" placeholder="Alamat" name="alamat" value="{{ old('alamat') }}">
  									</div>
  									@if($errors->has('alamat'))
  										<small class="help-block">
  											<strong>{{ $errors->first('alamat') }}</strong>
  										</small>
  									@endif
  								</div>
  								<div class="col-sm-6">
  									<div class="form-group{{ $errors->has('kodepos') ? ' has-error' : '' }}">
  										<input type="text" class="form-control" placeholder="Kode Pos" name="kodepos" value="{{ old('kodepos') }}">
  									</div>
  									@if($errors->has('kodepos'))
  										<small class="help-block">
  											<strong>{{ $errors->first('kodepos') }}</strong>
  										</small>
  									@endif
  								</div>
  		          </div>

  							<div class="row">
  								<div class="col-sm-6">
  									<span class="pull-left btn btn-sm btn-success btn-fill btn-file">
  								    	Upload Foto Terbaru
  								      <input type="file" name="img_profile" class="imageUpload">
												<span class="imageName"></span>
  									</span>
  									@if($errors->has('img_profile'))
  							      		<div class="has-error">
  										    <small class="help-block" style="display: inline-block;">
  												<strong>{{ $errors->first('img_profile') }}</strong>
  											</small>
  										</div>
  									@endif
  								</div>
  							</div>

  							<div class="row mar-top">
  		            <h3 class="h5 mar-ver" style="padding-left: 10px;"><span>Data KTP</span></h3>
  		            <div class="col-sm-12">
    								<div class="form-group{{ $errors->has('nik') ? ' has-error' : '' }}">
    									<input type="text" class="form-control" placeholder="NIK" name="nik" value="{{ old('nik') }}">
    								</div>
    								@if($errors->has('nik'))
    									<small class="help-block">
    										<strong>{{ $errors->first('nik') }}</strong>
    									</small>
    								@endif
  								</div>
  		          </div>

  		          <div class="row">
  								<div class="col-sm-6">
										<div class="form-group{{ $errors->has('provinsi_ktp') ? ' has-error' : '' }}">
  										<select name="provinsi_ktp" id="provinsi_ktp" class="form-control select2">
  											@foreach($provinsis as $provinsi)
  												<option value="{{ $provinsi->id }}">{{$provinsi->name}}</option>
  											@endforeach
  										</select>
  									</div>
  									@if($errors->has('provinsi_ktp'))
  										<small class="help-block">
  											<strong>{{ $errors->first('provinsi_ktp') }}</strong>
  										</small>
  									@endif
  								</div>
  								<div class="col-sm-6">
										<div class="form-group{{ $errors->has('kota_ktp') ? ' has-error' : '' }}">
  										<select name="kota_ktp" id="kabupaten_ktp" class="form-control select2">
  											<option value="">Pilih Kabupaten / Kota</option>
  										</select>
  									</div>
  									@if($errors->has('kota_ktp'))
  										<small class="help-block">
  											<strong>{{ $errors->first('kota_ktp') }}</strong>
  										</small>
  									@endif
  								</div>
  								<div class="col-sm-6">
										<div class="form-group{{ $errors->has('kecamatan_ktp') ? ' has-error' : '' }}">
	  									<select name="kecamatan_ktp" id="kecamatan_ktp" class="form-control select2">
	  										<option value="">Pilih Kecamatan</option>
	  									</select>
	  								</div>
  									@if($errors->has('kecamatan_ktp'))
  										<small class="help-block">
  											<strong>{{ $errors->first('kecamatan_ktp') }}</strong>
  										</small>
  									@endif
  								</div>
  								<div class="col-sm-6">
										<div class="form-group{{ $errors->has('kelurahan_ktp') ? ' has-error' : '' }}">
	  									<select name="kelurahan_ktp" id="kelurahan_ktp" class="form-control select2">
	  										<option value="">Pilih Kelurahan</option>
	  									</select>
	  								</div>
										@if($errors->has('kelurahan_ktp'))
  										<small class="help-block">
  											<strong>{{ $errors->first('kelurahan_ktp') }}</strong>
  										</small>
  									@endif
  								</div>
  							</div>

  							<div class="row">
  		              <div class="col-sm-6">
    									<div class="form-group{{ $errors->has('alamat_ktp') ? ' has-error' : '' }}">
    										<input type="text" class="form-control" placeholder="Alamat" name="alamat_ktp" value="{{ old('alamat_ktp') }}">
    									</div>
    									@if($errors->has('alamat_ktp'))
    										<small class="help-block">
    											<strong>{{ $errors->first('alamat_ktp') }}</strong>
    										</small>
    									@endif
  								  </div>
										<div class="col-sm-6">
    									<div class="form-group{{ $errors->has('kodepos_ktp') ? ' has-error' : '' }}">
    										<input type="text" class="form-control" placeholder="Kode Pos" name="kodepos_ktp" value="{{ old('kodepos_ktp') }}">
    									</div>
    									@if($errors->has('kodepos_ktp'))
    										<small class="help-block">
    											<strong>{{ $errors->first('kodepos_ktp') }}</strong>
    										</small>
    									@endif
  								  </div>
  		          </div>

		            <div class="row">
  								<div class="col-sm-6">
  									<span class="pull-left btn btn-sm btn-warning btn-fill btn-file" id="filename">
  								    	Upload Scan KTP
  								       	<input type="file" name="img_ktp" id="img_ktp">
  									</span>
  									@if($errors->has('img_ktp'))
  							      		<div class="has-error">
  										    <small class="help-block" style="display: inline-block;">
  												<strong>{{ $errors->first('img_ktp') }}</strong>
  											</small>
  										</div>
  									@endif
  								</div>
							  </div>
  							<div class="row">
  								<div class="col-sm-12">
  									<button type="submit" class="btn btn-info btn-block btn-fill btn-sm mar-top">Simpan</button>
  								</div>
  							</div>
						</form>
					</div>
				</div>
			</div>

		</div>
	</div>

@endsection
