<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateAgentRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'nama' => 'required|min:3',
          'jns_kelamin' => 'required',
          'telp' => 'required|min:3|digits_between:3,15',
          'tmptLahir' => 'required|min:3',
          'tglLahir' => 'required|date',
          'kota' => 'required|min:3',
          'kelurahan' => 'required|min:3',
          'kecamatan' => 'required|min:3',
          'alamat' => 'required|min:3',
          'kodepos' => 'required|min:3|digits_between:3,7',
          'img_profile' => 'required|file|image',
          'nik' => 'required|min:3|digits_between:3,20',
          'provinsi_ktp' => 'required',
          'kota_ktp' => 'required',
          'kelurahan_ktp' => 'required',
          'kecamatan_ktp' => 'required',
          'alamat_ktp' => 'required|min:3',
          'kodepos_ktp' => 'required|min:3|digits_between:3,7',
          'img_ktp' => 'required|file|image'
        ];
    }

    public function messages()
    {
      return [
          'nama.required' => 'Kolom nama Harus diisi',
          'nama.min' => 'Kolom nama Minimal 3 huruf',
          //'nama.alpha' => 'Kolom nama harus alfabet',
          'jns_kelamin.required' => 'Pilih jenis kelamin',
          'telp.required' => 'Kolom No telp harus diisi',
          'telp.min' => 'Kolom No telp minimal 3 digit',
          'telp.digits_between' => 'Kolom No telp harus minimal 3 - 15 digit',
          'tmptLahir.required' => 'Kolom tempat lahir harus diisi',
          'tmptLahir.min' => 'Kolom tempat lahir minimal 3 huruf',
          'tmptLahir.alpha' => 'Kolom tempat lahir harus alfabet',
          'tglLahir.required' => 'Kolom Tanggal lahir harus diisi',
          'tglLahir.date' => 'Kolom Tanggal lahir harus berupa tangal',
          'kota.required' => 'Kolom kota harus diisi',
          'kota.min' => 'Kolom kota minimal 3 huruf',
          'kota.alpha' => 'Kolom kota harus alfabet',
          'kelurahan.required' => 'Kolom kelurahan harus diisi',
          'kelurahan.min' => 'Kolom kelurahan minimal 3 huruf',
          'kelurahan.alpha' => 'Kolom kelurahan harus alfabet',
          'kecamatan.required' => 'Kolom kecamatan harus diisi',
          'kecamatan.min' => 'Kolom kecamatan minimal 3 huruf',
          'kecamatan.alpha' => 'Kolom kecamatan harus alfabet',
          'alamat.required' => 'Kolom alamat harus diisi',
          'alamat.min' => 'Kolom alamat minimal 3 huruf',
          'kodepos.required' => 'Kolom kodepos harus diisi',
          'kodepos.min' => 'Kolom kodepos minimal 3 digit',
          'kodepos.digits_between' => 'Kolom kodepos harus minimal 3 - 7 digit',
          'img_profile.required' => 'Kolom Foto harus diisi',
          'img_profile.image' => 'Kolom foto harus berupa (jpg,jpeg,png)',
          'nik.required' => 'Kolom NIK harus diisi',
          'nik.min' => 'Kolom NIK minimal 3 digit',
          'nik.digits_between' => 'Kolom NIK harus minimal 3 - 20 digit',
          'provinsi_ktp.required' => 'Koovinsi harus diisi',
          'provinsi_ktp.min' => 'Kolom Provinsi minimal 3 huruf',
          'provinsi_ktp.alpha' => 'Kolom Provinsi harus alfabet',
          'kota_ktp.required' => 'Kolom kota harus diisi',
          'kota_ktp.min' => 'Kolom kota minimal 3 huruf',
          'kota_ktp.alpha' => 'Kolom kota harus alfabet',
          'kelurahan_ktp.required' => 'Kolom kelurahan harus diisi',
          'kelurahan_ktp.min' => 'Kolom kelurahan minimal 3 huruf',
          'kelurahan_ktp.alpha' => 'Kolom kelurahan harus alfabet',
          'kelurahan_ktp.required' => 'Kolom kelurahan harus diisi',
          'kelurahan_ktp.min' => 'Kolom kelurahan minimal 3 huruf',
          'kelurahan_ktp.alpha' => 'Kolom kelurahan harus alfabet',
          'alamat_ktp.required' => 'Kolom alamat harus diisi',
          'alamat_ktp.min' => 'Kolom alamat minimal 3 huruf',
          'kodepos_ktp.required' => 'Kolom kodepos harus diisi',
          'kodepos_ktp.min' => 'Kolom kodepos minimal 3 digit',
          'kodepos_ktp.digits_between' => 'Kolom kodepos harus minimal 3 - 7 digit',
          'img_ktp.required' => 'Kolom Foto harus diisi',
          'img_ktp.image' => 'Kolom foto harus berupa (jpg,jpeg,png)'
      ];
    }
}
