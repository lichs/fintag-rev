<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'users';

    protected $fillable = [
      'username' ,'email', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function role()
    {
        return $this->belongsToMany(Role::class, 'roles_users');
    }

    public function hasRole($name)
    {
        foreach($this->role as $roles)
        {
            if ($roles->name === $name) return true;
        }

        return false;
    }

    public function assignRole($role)
    {
        if (is_string($role))
        {
            $role = Role::where('name', $role)->first();
        }

        return $this->role()->attach($role);
    }

    public function agent()
    {
        return $this->hasMany(Agent::class, 'user_id', 'id');
    }

    public function pelaku_usaha()
    {
        return $this->hasMany(PelakuUsaha::class, 'user_id', 'id');
    }
}
