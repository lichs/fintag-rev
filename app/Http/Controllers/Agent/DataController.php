<?php

namespace App\Http\Controllers\Agent;

use Datatables;
use Auth;
use App\RequestAgent;
use App\Agent;
use App\PelakuUsaha;
use App\PengajuanPu;
use App\User;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DataController extends Controller
{

    public function getRequestAgent()
    {
        $agents = Agent::where('user_id', Auth::user()->id)->get();

        foreach ($agents as $agent)
        {
            $request = RequestAgent::with('pelakuUsaha')->where('agent_id', $agent->id)
                                                        ->where('accepted', 0)
                                                        ->orderBy('created_at', 'asc')->get();
        }

        return Datatables::of($request)->addColumn('action', function($request){
                return '<a href="/agent/permintaan-pengajuan/terima/' . $request->id .'" class="btn btn-sm btn-info">Terima</a>
                        <a href="/agent/permintaan-pengajuan/tolak/' . $request->id .'" class="btn btn-sm btn-danger">Tolak</a>';
                })->addColumn('details_url', function($request) {
                    return url('/agent/data/detail-request/' . $request->pelaku_usaha_id);
                })->make(true);
    }

    public function getRequestHistory()
    {
        $agents = Agent::where('user_id', Auth::user()->id)->get();

        foreach ($agents as $agent)
        {
            $request = RequestAgent::with('pelakuUsaha')->where('agent_id', $agent->id)
                                                        ->where('accepted', '>' ,0)
                                                        ->orderBy('created_at', 'asc')->get();
        }

        return Datatables::of($request)->addColumn('status', function($request){

                if ($request->accepted == 1):
                  $status = '<span class="label label-success">Diterima</span>';
                else:
                  $status = '<span class="label label-danger">Ditolak</span>';
                endif;

                return $status;
                })->make(true);
    }

    public function getRequestAccepted()
    {
        $agents = Agent::where('user_id', Auth::user()->id)->get();

        foreach ($agents as $agent)
        {
            $request = RequestAgent::with('pelakuUsaha')->where('agent_id', $agent->id)
                                                        ->where('accepted', 1)
                                                        ->orderBy('created_at', 'asc')->get();
        }

        return Datatables::of($request)->addColumn('action', function($request){

                $state = RequestAgent::where('ticket_no', $request->ticket_no)->firstorFail();

                if($state->pengajuanPu()->get()->isEmpty()):

                    $status = '<a href="/agent/submit-pengajuan-badan-usaha/' . $request->id . '" class="btn btn-md btn-default add-tooltip" data-toggle="tooltip" data-container="body" data-placement="left"  data-original-title="Pengajuan Badan Usaha"><i class="fa fa-users"></i></a>
                    <a href="/agent/submit-pengajuan-perorangan/' . $request->id . '" class="btn btn-md btn-default"><i class="fa fa-user"></i></a>';

                else:

                    $dataPengajuan = PengajuanPu::with('JenisPengajuan')->where('request_id', $request->id)->get();

                    foreach ($dataPengajuan as $pengajuan)
                    {
                      $status = '<a href="/agent/pengajuan-' . str_slug($pengajuan->jenisPengajuan->nama, '-') . '/' . $pengajuan->id . '" class="btn btn-md btn-default"> Pengajuan ' . $pengajuan->jenisPengajuan->nama . '</a>';
                    }

                endif;

                return $status;

                })->make(true);
    }

    public function getDetailsRequestPengajuan($id)
    {
        $request = PengajuanPu::where('pelaku_usaha_id', $id)->get();

        return Datatables::of($request)->make(true);
    }

    public function getKabupatenByProvinsi($id)
    {
        $kabupaten = DB::table('kabupaten')->where('provinsi_id', $id)->get();
        return response()->json($kabupaten);
    }

    public function getKecamatanByKabupaten($id)
    {
        $kecamatan = DB::table('kecamatan')->where('kabupaten_id', $id)->get();
        return response()->json($kecamatan);
    }

    public function getKelurahanByKecamatan($id)
    {
        $kelurahan = DB::table('kelurahan')->where('kecamatan_id', $id)->get();
        return response()->json($kelurahan);
    }

    public function getPuByNik($nik)
    {
        $pu = PelakuUsaha::where('nik', $nik)->get();
        return response()->json($pu);
    }

    public function getAllAgentData($id)
    {
      $agent = Agent::where('user_id', $id)->with('Sertifikat')->get();

      foreach ($agent as $agents) {
          $kota_id = $agents->kota;
      }

      $kota = DB::table('kabupaten')->where('id', $kota_id)->select('name')->get();

      return response()->json(['agent' => $agent, 'kota' => $kota]);
    }
}
