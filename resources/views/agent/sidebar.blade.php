<nav id="mainnav-container">
	<div id="mainnav">
		<div id="mainnav-menu-wrap">
			<div class="nano">
				<div class="nano-content">
					<div id="mainnav-profile" class="mainnav-profile">
						<div class="profile-wrap">
							<div class="pad-btm">
								<span class="label label-success pull-right">Verified</span>
								<a href="profil.php">
								<img class="img-square img-sm img-border" src="{{ Storage::url('public/agent_profile/' . $agent->agent_image) }}" alt="Profile Picture">
								</a>
							</div>
							<p class="mnp-name">{{ $agent->nama }}</p>
							<span class="mnp-desc">{{ Auth::user()->email }}</span>
						</div>
					</div>

					<ul id="mainnav-menu" class="list-group">

						<div class="mainnav-widget">
							<li class="active-link">
							    <a href="{{ url('/agent') }}">
								    <i class="psi-home"></i>
								    <span class="menu-title">
										<strong>Beranda</strong>
									</span>
							    </a>
							</li>
						</div>

						<div class="mainnav-widget">
							<li class="list-header">Pengajuan Pembiayaan</li>
							<li>
								<a href="{{ url('agent/permintaan-pengajuan')}}">
								    <i class="psi-files"></i>
								    <span class="menu-title">
										<strong>Permintaan Pengajuan</strong>
									</span>
							    </a>
							</li>
							<li>
								<a href="{{ url('agent/data-pengajuan') }}">
								    <i class="psi-folder-open"></i>
								    <span class="menu-title">
										<strong>Data Pengajuan</strong>
									</span>
							    </a>
							</li>
							<li>
								<a href="#">
								    <i class="psi-box-with-folders"></i>
								    <span class="menu-title">
										<strong>Pelaku Usaha</strong>
									</span>
							    </a>
							</li>
							<li>
								<a href="{{ url('agent/pengajuan-setujui') }}">
								    <i class="psi-folder-bookmark"></i>
								    <span class="menu-title">
										<strong>Pengajuan disetujui</strong>
									</span>
							    </a>
							</li>
						</div>

						<div class="mainnav-widget">
							<li class="list-header">Bonus Fintag</li>
							<li>
								<a href="{{ url('agent/point') }}">
								    <i class="psi-smile"></i>
								    <span class="menu-title">
										<strong>Point & Rating</strong>
									</span>
							    </a>
							</li>
							<li>
								<a href="{{ url('agent/dompet') }}">
								    <i class="psi-wallet-2"></i>
								    <span class="menu-title">
										<strong>Dompet Saya</strong>
									</span>
							    </a>
							</li>
						</div>

						<div class="mainnav-widget">
							<li class="list-header">Bantuan dan Layanan</li>
							<li>
								<a href="{{ url('/agent/profile') }}">
								    <i class="fa fa-gear"></i>
								    <span class="menu-title">
										<strong>Pengaturan profile</strong>
									</span>
							    </a>
							</li>
							<li>
								<a href="#">
								    <i class="fa fa-question-circle-o"></i>
								    <span class="menu-title">
										<strong>F.A.Q</strong>
									</span>
							    </a>
							</li>
							<li>
								<a href="{{ url('/agent/logout') }}"
									onclick="event.preventDefault();
	                                document.getElementById('logout-form').submit();">
	                                <i class="fa fa-sign-out"></i>
	                                <span class="menu-title">
										<strong>Keluar</strong>
									</span>
	              </a>
	              <form id="logout-form" action="{{ url('/agent/logout') }}" method="POST" style="display: none;">
	                  {{ csrf_field() }}
	              </form>
							</li>
						</div>
					</ul>
				</div>
			</div>
		</div>
	</div>
</nav>
