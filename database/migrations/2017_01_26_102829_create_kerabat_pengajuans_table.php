<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKerabatPengajuansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kerabat_pengajuan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('identitas_pengajuan_perorangan_id')->unsigned();
            $table->string('nama');
            $table->string('hubungan');
            $table->string('alamat');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kerabat_pengajuan');
    }
}
