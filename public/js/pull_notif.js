$('#menu-pengajuan').hide();
$('#badge-notif').hide();

(function pulling_notif() {
	setTimeout(function() {
	    $.ajax({ url: 'data_notif.php', success: function(res) {
	    	// console.log(res);
	    	// navbar top
	    	$('#badge-notif').hide();
	    	$('#all-notif').empty();
	    	
	    	// left menu
	    	$('#menu-pengajuan').hide();

	    	// right sidebar
	    	$('#badge-for-request').hide();
	    	$('#list-of-request').empty();
	    	$('#badge-for-process').hide();
	    	$('#list-of-process').empty();

	    	// navbar top
	    	if (res.num_of_notif > 0) {
	    		$('#badge-notif').show();
	    		$('#number-notif').text(res.num_of_notif);
	    		
	    		for (var i=0; i<res.content_notif.length; i++) {
	    			var classAttr = '';

	    			if (res.content_notif[i].status == '1') {
	    				classAttr = 'class="bg-gray"';
	    			}

	    			$('#all-notif').append(
	    				'<li '+classAttr+'>' +
                            '<a class="media" href="#">' +
                                '<div class="media-body">' +
                                    '<div class="text-nowrap">'+res.content_notif[i].jenis+' '+res.content_notif[i].ref+'</div>' +
                                    '<small class="text-muted">'+res.content_notif[i].waktu+'</small>' +
                                '</div>' +
                            '</a>' +
                        '</li>'
	    			);
	    		}
	    	}

	    	// left menu
	    	if (res.notif_pengajuan > 0) {
	    		$('#menu-pengajuan').text(res.notif_pengajuan);
	    		$('#menu-pengajuan').show();
	    	}

	    	// right side bar
	    	// number of request
	    	if (res.num_of_request.length > 0) {
	    		$('#badge-for-request').text(res.num_of_request.length);
	    		$('#badge-for-request').show();

	    		for (var i = 0; i < res.num_of_request.length; i++) {
		    		$('#list-of-request').append(
		    			'<a href="index.html#" class="list-group-item">' +
			                '<div class="media-left">' +
			                    '<img class="img-circle img-xs" src="img/av2.png" alt="Profile Picture">' +
			                '</div>' +
			                '<div class="media-body">' +
			                    '<p class="mar-no">' +res.num_of_request[i].nama+ '</p>' +
			                    '<small class="text-muted">Availabe</small>' +
			                '</div>' +
			            '</a>'
		    		);
	    		}
	    	}

	    	// number of request
	    	if (res.num_of_process.length > 0) {
	    		$('#badge-for-process').text(res.num_of_process.length);
	    		$('#badge-for-process').show();

	    		for (var i = 0; i < res.num_of_process.length; i++) {
		    		$('#list-of-process').append(
		    			'<a href="index.html#" class="list-group-item">' +
			                '<div class="media-left">' +
			                    '<img class="img-circle img-xs" src="img/av2.png" alt="Profile Picture">' +
			                '</div>' +
			                '<div class="media-body">' +
			                    '<p class="mar-no">' +res.num_of_process[i].nama+ '</p>' +
			                    '<small class="text-muted">Availabe</small>' +
			                '</div>' +
			            '</a>'
		    		);
	    		}
	    	}

	    }, dataType: "json", complete: pulling_notif}); // jika complete memanggil kembali pulling_notif
	}, 30000); // 30000 miliscond atau 1/2 menit
})();