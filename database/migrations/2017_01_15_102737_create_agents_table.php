<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('agents', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('user_id')->unsigned();
          $table->string('nama');
          $table->integer('no_telp')->nullable();
          $table->string('jenis_kelamin', 10)->nullable();
          $table->string('tempat_lahir')->nullable();
          $table->string('tanggal_lahir')->nullable();
          $table->string('provinsi')->nullable();
          $table->string('kota')->nullable();
          $table->string('kelurahan')->nullable();
          $table->string('kecamatan')->nullable();
          $table->string('alamat')->nullable();
          $table->integer('kode_pos')->nullable();
          $table->string('agent_image')->nullable();
          $table->integer('nik')->nullable();
          $table->string('provinsi_ktp')->nullable();
          $table->string('kota_ktp')->nullable();
          $table->string('kelurahan_ktp')->nullable();
          $table->string('kecamatan_ktp')->nullable();
          $table->string('alamat_ktp')->nullable();
          $table->integer('kodepos_ktp')->nullable();
          $table->string('ktp_image')->nullable();
          $table->boolean('validate')->default(false);
          $table->timestamps();
      });

      Schema::table('agents', function($table){
          $table->foreign('user_id')->references('id')->on('users');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agents');
    }
}
