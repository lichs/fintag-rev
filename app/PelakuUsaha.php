<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PelakuUsaha extends Model
{
    protected $table = 'pelaku_usaha';

    protected $fillable = [
      'user_id' , 'nik' ,'nama' , 'no_telp' , 'jenis_kelamin' , 'nama_ibu_kandung' , 'kota' ,
      'kelurahan', 'kecamatan' , 'alamat' , 'kode_pos' , 'pendidikan_terakhir' ,
      'status' , 'password_temporary'
    ];

    public function user()
    {
    	return $this->belongsTo(User::class, 'user_id');
    }
}
