@extends('layouts.app')

@section('content')
  <div class="effect aside-float aside-bright mainnav-lg" id="container">
    <div id="bg-overlay" class="bg-img img-balloon1"></div>

    <div class="cls-content">
      <div class="cls-content-sm panel">
              <div class="panel-body">
                  <div class="mar-ver pad-btm">
                      <h3 class="text-center mar-no">Register Agent</h3>
                  </div>
                  <form method="POST" action="{{ url('/agent/register') }}">
                      {{ csrf_field() }}
                      <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                          <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" autofocus placeholder="Email Anda">
                          @if ($errors->has('email'))
                              <span class="help-block">
                                  <strong>{{ $errors->first('email') }}</strong>
                              </span>
                          @endif
                      </div>
                      <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                        <input id="username" type="text" class="form-control" name="username" placeholder="Username Anda">
                        @if ($errors->has('username'))
                            <span class="help-block">
                                <strong>{{ $errors->first('username') }}</strong>
                            </span>
                        @endif
                      </div>
                      <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <input id="password" type="password" class="form-control" name="password" placeholder="Password Anda">
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                      </div>
                      <div class="form-group">
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password Anda">
                      </div>
                      <div class="form-group">
                        <button type="submit" class="btn btn-fill btn-block btn-primary">
                                  Register
                        </button>
                      </div>
                      <p class="text-muted mar-no text-sm">
                        Sudah Punya akun ? <span><a href="{{ url('/agent/login') }}"> Login</a></span>
                      </p>
                  </form>
              </div>
      </div>
    </div>

  </div>
@endsection
