@extends('layouts.app')

@section('content')
	<div id="container" class="effect aside-float aside-bright mainnav-lg">

	@foreach($agents as $agent)

		@include('agent.navigation')
			<div class="boxed">
				<div id="content-container">
					<div id="page-content">
						<div class="panel">
							<div class="panel-body pad-no">
								<h3 class="panel-title">Beranda</h3>
							</div>
							<div class="panel-body pad-ver">
								<div class="row">
									<div class="col-sm-3 col-lg-3">
										<div class="panel panel-info panel-colorful">
											<div class="pad-all">
												<p class="text-lg text-semibold"><i class="demo-pli-wallet-2 icon-fw"></i> Pengajuan Sukses</p>
												<p class="mar-no">
													<span class="pull-right text-bold">0</span>
													Total
												</p>
											</div>
										</div>
									</div>
									<div class="col-sm-3 col-lg-3">
										<div class="panel panel-purple panel-colorful">
											<div class="pad-all">
												<p class="text-lg text-semibold"><i class="demo-pli-bag-coins icon-fw"></i> Pesan</p>
												<p class="mar-no">
													<span class="pull-right text-bold">24</span>
													Kotak Masuk
												</p>
											</div>
										</div>
									</div>
									<div class="col-sm-3 col-lg-3">
										<div class="panel panel-success panel-colorful">
											<div class="pad-all">
												<p class="text-lg text-semibold"><i class="demo-pli-bag-coins icon-fw"></i> Pengajuan</p>
												<p class="mar-no">
													Total Pengajuan
													<span class="pull-right text-bold">24</span>
												</p>
											</div>
										</div>
									</div>
									<div class="col-sm-3 col-lg-3">
										<div class="panel panel-success panel-colorful">
											<div class="pad-all">
												<p class="text-lg text-semibold"><i class="demo-pli-bag-coins icon-fw"></i> Poin Anda</p>
												<p class="mar-no">
													Total Poin
													<span class="pull-right text-bold">24</span>
												</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-xs-12">
								<div class="panel">
									<div class="panel-heading">
										<h3 class="panel-title">Permintaan Pengajuan</h3>
									</div>
									<div class="panel-body">
										<div class="table-responsive">
											<table class="table table-striped">
												<thead>
													<tr>
														<th>Nomor Pengajuan</th>
														<th>Nama</th>
														<th>Tanggal</th>
														<th class="text-center">Status</th>

													</tr>
												</thead>
												<tbody>
													<tr>
														<td><a href="#" class="btn-link"> Order #53431</a></td>
														<td>Steve N. Horton</td>
														<td><span class="text-muted"><i class="fa fa-clock-o"></i> Oct 22, 2014</span></td>
														<td class="text-center">
															<div class="label label-table label-success">Paid</div>
														</td>
														<td><a href="" class="btn-link">Lihat Pengajuan</a></td>
													</tr>
													<tr>
														<td><a href="#" class="btn-link"> Order #53431</a></td>
														<td>Steve N. Horton</td>
														<td><span class="text-muted"><i class="fa fa-clock-o"></i> Oct 22, 2014</span></td>
														<td class="text-center">
															<div class="label label-table label-success">Paid</div>
														</td>
														<td><a href="" class="btn-link">Lihat Pengajuan</a></td>
													</tr>
													<tr>
														<td><a href="#" class="btn-link"> Order #53431</a></td>
														<td>Steve N. Horton</td>
														<td><span class="text-muted"><i class="fa fa-clock-o"></i> Oct 22, 2014</span></td>
														<td class="text-center">
															<div class="label label-table label-success">Paid</div>
														</td>
														<td><a href="" class="btn-link">Lihat Pengajuan</a></td>
													</tr>
													<tr>
														<td><a href="#" class="btn-link"> Order #53431</a></td>
														<td>Steve N. Horton</td>
														<td><span class="text-muted"><i class="fa fa-clock-o"></i> Oct 22, 2014</span></td>
														<td class="text-center">
															<div class="label label-table label-success">Paid</div>
														</td>
														<td><a href="" class="btn-link">Lihat Pengajuan</a></td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				@include('agent.sidebar')
			</div>

	@endforeach
		</div>
@endsection
