<?php

namespace App\Http\Controllers\Agent;

use Alert;
use Auth;
use App\Agent;
use App\User;
use App\PelakuUsaha;
use App\RequestAgent;
use Carbon\Carbon;
use App\Sertifikat;
use App\Http\Requests\CreateAgentRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AgentController extends Controller
{
    public function createAgent(CreateAgentRequest $request)
    {
      $agent_image_name = 'agent_profile_' . $request->user()->id . '.' . $request->img_profile
              ->extension();

      $agent_ktp_image_name = 'agent_ktp_image_' . $request->user()->id . '.' . $request->img_ktp->extension();

      Agent::create([
            'user_id' => $request->user()->id,
            'nama' => $request->nama,
            'jenis_kelamin' => $request->jns_kelamin,
            'no_telp' => $request->telp,
            'tempat_lahir' => $request->tmptLahir,
            'tanggal_lahir' => $request->tglLahir,
            'kota' => $request->kota,
            'kelurahan' => $request->kelurahan,
            'kecamatan' => $request->kecamatan,
            'alamat' => $request->alamat,
            'kode_pos' => $request->kodepos,
            'agent_image' => $agent_image_name,
            'nik' => $request->nik,
            'kota_ktp' => $request->kota_ktp,
            'kelurahan_ktp' => $request->kelurahan_ktp,
            'kecamatan_ktp' => $request->kecamatan_ktp,
            'alamat_ktp' => $request->alamat_ktp,
            'kodepos_ktp' => $request->kodepos_ktp,
            'ktp_image' => $agent_ktp_image_name
      ]);

      $agent_image = $request->img_profile->storeAs('public/agent_profile', $agent_image_name);

      $agent_ktp_image = $request->img_profile->storeAs('public/agent_ktp', $agent_ktp_image_name);

      return redirect('agent/sertifikat');
    }

    public function createSertifikatAgent(Request $request)
    {
      $sertifikat_image_name = 'sertifikat_image_' . $request->nama . '.' . $request->sertifikat_image->extension();

      $agents = Agent::where('user_id', $request->user()->id)->get();

      foreach ($agents as $agent) {
        Sertifikat::create([
          'agent_id' => $agent->id,
          'no_sertifikat' => $request->no_sertifikat,
          'tanggal_awal' => Carbon::parse($request->tgl_awal),
          'tanggal_akhir' => $request->tgl_akhir,
          'sertifikat_image' => $sertifikat_image_name
        ]);
      }

      $request->sertifikat_image->storeAs('public/agent_sertifikat', $sertifikat_image_name);

      return redirect('/agent');
    }

    public function createPermintaanPengajuan(Request $request)
    {
        $pu = PelakuUsaha::where('nik', $request->nik)->get();
        $username_temporary = substr($request->nama, 4) . "" . str_random(4);
        $password_temporary = str_random(8);
        $agents = Auth::user()->agent;

        foreach ($agents as $agent) {
            $agent_id = $agent->id;
        }

        if ($pu->isEmpty()) {
            $user = User::create([
                'username' => $username_temporary,
                'email' => $request->email,
                'password' => bcrypt($password_temporary)
            ]);

            $user->assignRole('pu');

            $pelaku = PelakuUsaha::create([
                'user_id' => $user->id,
                'nama' => $request->nama,
                'nik' => $request->nik,
                'alamat' => $request->alamat,
                'no_telp' => $request->no_telp,
                'password_temporary' => $password_temporary
            ]);

            RequestAgent::create([
              'ticket_no' => $this->generateTicketNumber(),
              'pelaku_usaha_id' => $pelaku->id,
              'agent_id' => $agent_id,
              'trigger_by' => 1,
              'accepted' => 0
            ]);

        } else {

          foreach ($pu as $pelaku) {

            RequestAgent::create([
              'ticket_no' => $this->generateTicketNumber(),
              'pelaku_usaha_id' => $pelaku->id,
              'agent_id' => $agent_id,
              'trigger_by' => 1,
              'accepted' => 0
            ]);

          }

        }

        return redirect('/agent/permintaan-pengajuan')->with('message','created');
    }

    function genRandomString($length = null)
    {
	    $characters = 'abcdefghijklmnopqrstuvwxyz';
	    $string = '';

	    for ($p = 0; $p < $length; $p++) {
	        $string .= $characters[rand(0, strlen($characters))];
	    }

	    return $string;
	  }

    function genRandomInteger($length = null)
    {
	    $characters = '0123456789';
	    $string = '';

	    for ($p = 0; $p < $length; $p++) {
	        $string .= $characters[rand(0, strlen($characters))];
	    }

	    return $string;
	  }

    private function generateTicketNumber()
    {
        $dt = Carbon::now();

        $ticketString = $dt->year . "" . $dt->month . "" . strtoupper(str_random(4));

        return $ticketString;
    }

    public function updateAgent($id, Request $request)
    {
      $agent = Agent::where('id',$id);
      //dd($agent);
      $agent->update([
        'nama' => $request->nama,
        'no_telp' => $request->no_telp,
        'nik' => $request->nik
      ]);

      return redirect('/agent/profile')->with('message', 'sukses');
    }
}
