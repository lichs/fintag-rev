@extends('layouts.app')

@section('content')

	<div id="container" class="effect aside-float aside-bright mainnav-lg">

		@foreach($agents as $agent)
			<div class="boxed">
				@include('agent.navigation')
					<div id="content-container">
						<div id="page-content">
							<div class="panel">
								<div class="panel-body pad-no">
									<h3 class="panel-title">Point Rating</h3>
									<ol class="breadcrumb">
										<li class="home">Beranda</li>
										<li class="active">Point Rating</li>
									</ol>
								</div>
								<div class="panel-body">
									<fieldset>
										<div class="form-group">
											<label class="col-sm-1 control-label mar-ver" >Dari</label>
											<div class="col-sm-3 mar-ver">
												<div class="input-group date">
													<input type="text" placeholder="Pilih Tanggal Awal"  class="form-control dtpicker">
													<span class="input-group-addon"><i class="pli-calendar-4"></i></span>
												</div>
											</div>
											<label class="col-sm-1 control-label mar-ver" >Sampai</label>
											<div class="col-sm-3 mar-ver">
												<div class="input-group date">
													<input type="text" placeholder="Pilih Tanggal Akhir"  class="form-control dtpicker">
													<span class="input-group-addon"><i class="pli-calendar-4"></i></span>
												</div>
											</div>
											<div class="col-sm-1 mar-ver">
												<button class="btn btn-default btn-icon" type="submit"><i class="fa fa-search"></i></button>
											</div>
										</div>
									</fieldset>
									<div class="clearfix mar-ver"></div>
								    <table id="demo-custom-toolbar" class="demo-add-niftycheck" data-toggle="table"
								        data-url="data/bs-table.json"
								        data-search="true"
								        data-show-refresh="true"
								        data-show-toggle="true"
								        data-show-columns="true"
								        data-sort-name="id"
								        data-page-list="[10, 20, 30]"
								        data-page-size="10"
								        data-pagination="true" data-show-pagination-switch="true">
									    <thead>
									        <tr>
									            <th data-field="id" data-sortable="true">No Tiket</th>
									            <th data-sortable="true" data-visible="false">Email Pelaku Usaha</th>
									            <th data-sortable="true">Nama Pelaku Usaha	</th>
									            <th data-sortable="true" data-visible="false">Alamat</th>
									            <th data-sortable="true">No. Telp</th>
									            <th data-sortable="true">Tgl. Request</th>
									            <th data-sortable="true">Jenis Pengajuan</th>
									        </tr>
									    </thead>
									</table>
								</div>
							</div>
						</div>
					</div>
				@include('agent.sidebar')
			</div>

		@endforeach

	</div>

@endsection
