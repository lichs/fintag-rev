<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IdentitasPengajuanPerorangan extends Model
{
    protected $table = 'identitas_pengajuan_perorangan';

    protected $fillable = [
      'id', 'pengajuan_id', 'ktp_image', 'npwp', 'npwp_image', 'status_npwp', 'status_pernikahan', 'alamat_ktp', 'alamat', 'status_kepemilikan_rumah', 'lama_tahun_menempati_rumah',
      'lama_bulan_menempati_rumah', 'status_kartu_keluarga', 'kartu_keluarga_image', 'status_akta_nikah_cerai', 'akta_nikah_cerai_image', 'status_ktp_pasangan',
      'ktp_pasangan_image', 'status_siup', 'siup_image', 'status_tdp', 'tdp_image', 'status_skdp_situ',
      'skdp_situ_image', 'status_akte_pendirian', 'akte_pendirian_image', 'status_ijin_usaha', 'ijin_usaha_image', 'status_kontrak', 'kontrak_image', 'status_nasabah_bank', 'rek_tabungan_image', 'status_laporan_keungan', 'laporan_keuangan_image', 'status_cash_flow', 'cash_flow_image',
      'status_daftar_hutang', 'daftar_hutang_image', 'status_daftar_mesin', 'daftar_alat_image', 'berat_mesin', 'status_daftar_penjamin', 'daftar_penjamin_image', 'status_daftar_keuangan_penjamin',
      'daftar_keuangan_penjamin_image'
    ];

    public function pengajuanPu()
    {
        return $this->belongsTo(PengajuanPu::class);
    }
}
