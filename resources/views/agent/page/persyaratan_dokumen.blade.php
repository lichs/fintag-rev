@extends('layouts.app')

@section('content')
	<div id="container" class="effect aside-float aside-bright mainnav-lg">
		@foreach($agents as $agent)
			<div class="boxed">
				@include('agent.navigation')
					<div id="content-container">
					  <div id="page-content">
              <div class="panel">
              	<div class="panel-body pad-no">
                    <h3 class="panel-title">Form Pengajuan Perorangan</h3>
                    <ol class="breadcrumb">
                      <li class="home">Beranda</li>
                      <li><a href="#">Pendataan Pengajuan</a></li>
                      <li>Form Pengajuan Badan Usaha</li>
                      <li class="active">Persyaratan Dokumen</li>
                    </ol>
              	</div>
                <div class="panel-body">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam mauris eros, dictum id nisi convallis, efficitur ultrices mauris. In condimentum eu risus eu gravida. In hac habitasse platea dictumst. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Suspendisse finibus mollis magna, at porttitor felis dictum et. Nulla sit amet est velit. Donec malesuada urna vel nisl faucibus, sed laoreet urna porta. </p>
                </div>
              </div>
              <div class="row">
                  <div class="col-lg-12">
                      <div class="panel">
                          <div class="panel-heading">
                              <h3 class="panel-title">Persyaratan Dokumen</h3>
                          </div>
                          <form class="panel-body form-horizontal" action="index.html" method="post">
                              <div class="col-sm-12">
                                  <h3 class="h5 mar-ver"><span>Data Proposal</span></h3>
                                  <div class="pad-hor">
                                      <div class="col-sm-10">
                                          <div class="form-group">
                                              <label class="col-md-4 control-label" >File Proposal *</label>
                                              <div class="col-md-5">
                                                  <input type="file" id="file_proposal" class="form-control" name="file_proposal"  />
                                              </div>
                                          </div>
                                      </div>
                                      <div class="col-sm-10">
                                          <div class="form-group">
                                              <label class="col-md-4 control-label" >Keterangan *</label>
                                              <div class="col-md-8">
                                                  <input type="text"  class="form-control" >
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                              <div class="col-sm-12">
                                  <h3 class="h5 mar-ver"><span>Persyaratan Dokumen</span></h3>
                                  <div class="pad-hor">
                                      <div class="col-sm-10">
                                          <div class="form-group">
                                              <label class="col-md-4 control-label" >KTP Pemohon &amp; Pasangan (Istri/Suami) *</label>
                                              <div class="col-md-4">
                                                  <div class="radio">
                                                      <input id="rbtDoku1-1" class="magic-radio" type="radio" name="rbtDoku1"  onClick="setformDoku1(this.value)" value="Ada" checked>
                                                      <label for="rbtDoku1-1">Ada</label>
                                                      <input id="rbtDoku1-2" class="magic-radio" type="radio" name="rbtDoku1"  onClick="setformDoku1(this.value)" value="Tidak">
                                                      <label for="rbtDoku1-2">Tidak</label>
                                                  </div>
                                              </div>
                                              <div class="col-md-5" id="formDoku1">
                                                  <input type="file" id="file_doku1" class="form-control" name="file_doku1"  />
                                              </div>
                                          </div>
                                      </div>
                                      <div class="col-sm-10">
                                          <div class="form-group">
                                              <label class="col-md-4 control-label" >KTP Direktur &amp; Komisaris *</label>
                                              <div class="col-md-4">
                                                  <div class="radio">
                                                      <input id="rbtDoku2-1" class="magic-radio" type="radio" name="rbtDoku2"  onClick="setformDoku2(this.value)"  value="Ada" checked>
                                                      <label for="rbtDoku2-1">Ada</label>
                                                      <input id="rbtDoku2-2" class="magic-radio" type="radio" name="rbtDoku2"   onClick="setformDoku2(this.value)"  value="Tidak">
                                                      <label for="rbtDoku2-2">Tidak</label>
                                                  </div>
                                              </div>
                                              <div class="col-md-5" id="formDoku2">
                                                  <input type="file" id="file_doku2" class="form-control" name="file_doku2"/>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="col-sm-10">
                                          <div class="form-group">
                                              <label class="col-md-4 control-label" >Kartu Keluarga *</label>
                                              <div class="col-md-4">
                                                  <div class="radio">
                                                      <input id="rbtDoku2-1" class="magic-radio" type="radio" name="rbtDoku2"  onClick="setformDoku2(this.value)"  value="Ada" checked>
                                                      <label for="rbtDoku2-1">Ada</label>
                                                      <input id="rbtDoku2-2" class="magic-radio" type="radio" name="rbtDoku2"   onClick="setformDoku2(this.value)"  value="Tidak">
                                                      <label for="rbtDoku2-2">Tidak</label>
                                                  </div>
                                              </div>
                                              <div class="col-md-5" id="formDoku2">
                                                  <input type="file" id="file_doku2" class="form-control" name="file_doku2"/>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="col-sm-10">
                                          <div class="form-group">
                                              <label class="col-md-4 control-label" >Akta Nikah / Cerai *</label>
                                              <div class="col-md-4">
                                                  <div class="radio">
                                                      <input id="rbtDoku2-1" class="magic-radio" type="radio" name="rbtDoku2"  onClick="setformDoku2(this.value)"  value="Ada" checked>
                                                      <label for="rbtDoku2-1">Ada</label>
                                                      <input id="rbtDoku2-2" class="magic-radio" type="radio" name="rbtDoku2"   onClick="setformDoku2(this.value)"  value="Tidak">
                                                      <label for="rbtDoku2-2">Tidak</label>
                                                  </div>
                                              </div>
                                              <div class="col-md-5" id="formDoku2">
                                                  <input type="file" id="file_doku2" class="form-control" name="file_doku2"/>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="col-sm-10">
                                          <div class="form-group">
                                              <label class="col-md-4 control-label" >NPWP *</label>
                                              <div class="col-md-4">
                                                  <div class="radio">
                                                      <input id="rbtDoku2-1" class="magic-radio" type="radio" name="rbtDoku2"  onClick="setformDoku2(this.value)"  value="Ada" checked>
                                                      <label for="rbtDoku2-1">Ada</label>
                                                      <input id="rbtDoku2-2" class="magic-radio" type="radio" name="rbtDoku2"   onClick="setformDoku2(this.value)"  value="Tidak">
                                                      <label for="rbtDoku2-2">Tidak</label>
                                                  </div>
                                              </div>
                                              <div class="col-md-5" id="formDoku2">
                                                  <input type="file" id="file_doku2" class="form-control" name="file_doku2"/>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="col-sm-10">
                                          <div class="form-group">
                                              <label class="col-md-4 control-label" >SIUP *</label>
                                              <div class="col-md-4">
                                                  <div class="radio">
                                                      <input id="rbtDoku2-1" class="magic-radio" type="radio" name="rbtDoku2"  onClick="setformDoku2(this.value)"  value="Ada" checked>
                                                      <label for="rbtDoku2-1">Ada</label>
                                                      <input id="rbtDoku2-2" class="magic-radio" type="radio" name="rbtDoku2"   onClick="setformDoku2(this.value)"  value="Tidak">
                                                      <label for="rbtDoku2-2">Tidak</label>
                                                  </div>
                                              </div>
                                              <div class="col-md-5" id="formDoku2">
                                                  <input type="file" id="file_doku2" class="form-control" name="file_doku2"/>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="col-sm-10">
                                          <div class="form-group">
                                              <label class="col-md-4 control-label" >TDP *</label>
                                              <div class="col-md-4">
                                                  <div class="radio">
                                                      <input id="rbtDoku2-1" class="magic-radio" type="radio" name="rbtDoku2"  onClick="setformDoku2(this.value)"  value="Ada" checked>
                                                      <label for="rbtDoku2-1">Ada</label>
                                                      <input id="rbtDoku2-2" class="magic-radio" type="radio" name="rbtDoku2"   onClick="setformDoku2(this.value)"  value="Tidak">
                                                      <label for="rbtDoku2-2">Tidak</label>
                                                  </div>
                                              </div>
                                              <div class="col-md-5" id="formDoku2">
                                                  <input type="file" id="file_doku2" class="form-control" name="file_doku2"/>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="col-sm-10">
                                          <div class="form-group">
                                              <label class="col-md-4 control-label" >SKDP / SITU *</label>
                                              <div class="col-md-4">
                                                  <div class="radio">
                                                      <input id="rbtDoku2-1" class="magic-radio" type="radio" name="rbtDoku2"  onClick="setformDoku2(this.value)"  value="Ada" checked>
                                                      <label for="rbtDoku2-1">Ada</label>
                                                      <input id="rbtDoku2-2" class="magic-radio" type="radio" name="rbtDoku2"   onClick="setformDoku2(this.value)"  value="Tidak">
                                                      <label for="rbtDoku2-2">Tidak</label>
                                                  </div>
                                              </div>
                                              <div class="col-md-5" id="formDoku2">
                                                  <input type="file" id="file_doku2" class="form-control" name="file_doku2"/>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="col-sm-10">
                                          <div class="form-group">
                                              <label class="col-md-4 control-label" >Akta Pendirian & Perubahan Berikut SK Menkeh *</label>
                                              <div class="col-md-4">
                                                  <div class="radio">
                                                      <input id="rbtDoku2-1" class="magic-radio" type="radio" name="rbtDoku2"  onClick="setformDoku2(this.value)"  value="Ada" checked>
                                                      <label for="rbtDoku2-1">Ada</label>
                                                      <input id="rbtDoku2-2" class="magic-radio" type="radio" name="rbtDoku2"   onClick="setformDoku2(this.value)"  value="Tidak">
                                                      <label for="rbtDoku2-2">Tidak</label>
                                                  </div>
                                              </div>
                                              <div class="col-md-5" id="formDoku2">
                                                  <input type="file" id="file_doku2" class="form-control" name="file_doku2"/>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="col-sm-10">
                                          <div class="form-group">
                                              <label class="col-md-4 control-label" >Izin Operasi Usaha / Izin Penting Lainnya *</label>
                                              <div class="col-md-4">
                                                  <div class="radio">
                                                      <input id="rbtDoku2-1" class="magic-radio" type="radio" name="rbtDoku2"  onClick="setformDoku2(this.value)"  value="Ada" checked>
                                                      <label for="rbtDoku2-1">Ada</label>
                                                      <input id="rbtDoku2-2" class="magic-radio" type="radio" name="rbtDoku2"   onClick="setformDoku2(this.value)"  value="Tidak">
                                                      <label for="rbtDoku2-2">Tidak</label>
                                                  </div>
                                              </div>
                                              <div class="col-md-5" id="formDoku2">
                                                  <input type="file" id="file_doku2" class="form-control" name="file_doku2"/>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="col-sm-10">
                                          <div class="form-group">
                                              <label class="col-md-4 control-label" >Kontrak Kerja / Penjualan / Surat Pesanan *</label>
                                              <div class="col-md-4">
                                                  <div class="radio">
                                                      <input id="rbtDoku2-1" class="magic-radio" type="radio" name="rbtDoku2"  onClick="setformDoku2(this.value)"  value="Ada" checked>
                                                      <label for="rbtDoku2-1">Ada</label>
                                                      <input id="rbtDoku2-2" class="magic-radio" type="radio" name="rbtDoku2"   onClick="setformDoku2(this.value)"  value="Tidak">
                                                      <label for="rbtDoku2-2">Tidak</label>
                                                  </div>
                                              </div>
                                              <div class="col-md-5" id="formDoku2">
                                                  <input type="file" id="file_doku2" class="form-control" name="file_doku2"/>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                              </div>

                              <div class="col-sm-12">
                                  <h3 class="h5 mar-ver"><span>Rekening Tabungan / Koran</span></h3>
                                  <div class="pad-hor">
                                      <div class="col-sm-10">
                                          <div class="form-group">
                                              <label class="col-md-4 control-label" >Nasabah Baru 6 Bulan Terakhir *</label>
                                              <div class="col-md-4">
                                                  <div class="radio">
                                                      <input id="rbtDoku12-1" class="magic-radio" type="radio" name="rbtDoku12"  onClick="setformDoku12(this.value)" value="Ada">
                                                      <label for="rbtDoku12-1">Ada</label>
                                                      <input id="rbtDoku12-2" class="magic-radio" type="radio" name="rbtDoku12"  onClick="setformDoku12(this.value)" value="Tidak" checked>
                                                      <label for="rbtDoku12-2">Tidak</label>
                                                  </div>
                                              </div>
                                              <div id="formDoku12">
                                                  <div class="col-md-5">
                                                      <input type="file" id="file_doku12" class="form-control" name="file_doku12"  />
                                                  </div>
                                              </div>
                                          </div>
                                      </div>

                                      <div class="col-sm-10">
                                          <div class="form-group">
                                              <label class="col-md-4 control-label" >Nasabah Baru 3 Bulan Terakhir *</label>
                                              <div class="col-md-4">
                                                  <div class="radio">
                                                      <input id="rbtDoku13-1" class="magic-radio" type="radio" name="rbtDoku13"  onClick="setformDoku13(this.value)" value="Ada">
                                                      <label for="rbtDoku13-1">Ada</label>
                                                      <input id="rbtDoku13-2" class="magic-radio" type="radio" name="rbtDoku13"  onClick="setformDoku13(this.value)" value="Tidak" checked>
                                                      <label for="rbtDoku13-2">Tidak</label>
                                                  </div>
                                              </div>
                                              <div id="formDoku13">
                                                  <div class="col-md-5">
                                                    <input type="file" id="file_doku13" class="form-control" name="file_doku13"  />
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                              </div>

                              <div class="col-sm-12">
                                  <h3 class="h5 mar-ver"><span>Laporan Keuangan 3 Bulan Terakhir</span></h3>
                                  <div class="pad-hor">
                                      <div class="col-sm-10">
                                          <div class="form-group">
                                              <label class="col-md-4 control-label" >Diaudit *</label>
                                              <div class="col-md-4">
                                                  <div class="radio">
                                                      <input id="rbtDoku14-1" class="magic-radio" type="radio" name="rbtDoku14"  onClick="setformDoku14(this.value)" value="Ada">
                                                      <label for="rbtDoku14-1">Ada</label>
                                                      <input id="rbtDoku14-2" class="magic-radio" type="radio" name="rbtDoku14"  onClick="setformDoku14(this.value)" value="Tidak" checked>
                                                      <label for="rbtDoku14-2">Tidak</label>
                                                  </div>
                                              </div>
                                              <div id="formDoku14">
                                                  <div class="col-md-5">
                                                  <input type="file" id="file_doku14" class="form-control" name="file_doku14"  />
                                                  </div>
                                              </div>
                                          </div>
                                      </div>

                                      <div class="col-sm-10">
                                          <div class="form-group">
                                              <label class="col-md-4 control-label" >Tidak Diaudit *</label>
                                              <div class="col-md-4">
                                                  <div class="radio">
                                                      <input id="rbtDoku15-1" class="magic-radio" type="radio" name="rbtDoku15"  onClick="setformDoku15(this.value)" value="Ada">
                                                      <label for="rbtDoku15-1">Ada</label>
                                                      <input id="rbtDoku15-2" class="magic-radio" type="radio" name="rbtDoku15"  onClick="setformDoku15(this.value)" value="Tidak" checked>
                                                      <label for="rbtDoku15-2">Tidak</label>
                                                  </div>
                                              </div>
                                              <div id="formDoku15">
                                                  <div class="col-md-5">
                                                    <input type="file" id="file_doku15" class="form-control" name="file_doku15"/>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>

                                      <div class="col-sm-10">
                                          <div class="form-group">
                                              <label class="col-md-4 control-label" >Proyeksi Cash Flow *</label>
                                              <div class="col-md-4">
                                                  <div class="radio">
                                                      <input id="rbtDoku16-1" class="magic-radio" type="radio" name="rbtDoku16"  onClick="setformDoku16(this.value)" value="Ada">
                                                      <label for="rbtDoku16-1">Ada</label>
                                                      <input id="rbtDoku16-2" class="magic-radio" type="radio" name="rbtDoku16"  onClick="setformDoku16(this.value)" value="Tidak" checked>
                                                      <label for="rbtDoku16-2">Tidak</label>
                                                  </div>
                                              </div>
                                              <div id="formDoku16">
                                                  <div class="col-md-5">
                                                      <input type="file" id="file_doku16" class="form-control" name="file_doku16"  />
                                                  </div>
                                              </div>
                                          </div>
                                      </div>

                                      <div class="col-sm-10">
                                          <div class="form-group">
                                              <label class="col-md-4 control-label" >Daftar Hutang Kreditur (Bank &amp; Leasing)*</label>
                                              <div class="col-md-4">
                                                  <div class="radio">
                                                      <input id="rbtDoku16-1" class="magic-radio" type="radio" name="rbtDoku16"  onClick="setformDoku16(this.value)" value="Ada">
                                                      <label for="rbtDoku16-1">Ada</label>
                                                      <input id="rbtDoku16-2" class="magic-radio" type="radio" name="rbtDoku16"  onClick="setformDoku16(this.value)" value="Tidak" checked>
                                                      <label for="rbtDoku16-2">Tidak</label>
                                                  </div>
                                              </div>
                                              <div id="formDoku16">
                                                  <div class="col-md-5">
                                                      <input type="file" id="file_doku16" class="form-control" name="file_doku16"  />
                                                  </div>
                                              </div>
                                          </div>
                                      </div>

                                      <div class="col-sm-10">
                                          <div class="form-group">
                                              <label class="col-md-4 control-label" >Daftar Alat Berat Dan/ Atau Mesin *</label>
                                              <div class="col-md-4">
                                                  <div class="radio">
                                                      <input id="rbtDoku16-1" class="magic-radio" type="radio" name="rbtDoku16"  onClick="setformDoku16(this.value)" value="Ada">
                                                      <label for="rbtDoku16-1">Ada</label>
                                                      <input id="rbtDoku16-2" class="magic-radio" type="radio" name="rbtDoku16"  onClick="setformDoku16(this.value)" value="Tidak" checked>
                                                      <label for="rbtDoku16-2">Tidak</label>
                                                  </div>
                                              </div>
                                              <div id="formDoku16">
                                                  <div class="col-md-5">
                                                      <input type="file" id="file_doku16" class="form-control" name="file_doku16"  />
                                                  </div>
                                              </div>
                                          </div>
                                      </div>

                                      <div class="col-sm-10">
                                          <div class="form-group">
                                              <label class="col-md-4 control-label" >Daftar Penjamin (jika ada) *</label>
                                              <div class="col-md-4">
                                                  <div class="radio">
                                                      <input id="rbtDoku16-1" class="magic-radio" type="radio" name="rbtDoku16"  onClick="setformDoku16(this.value)" value="Ada">
                                                      <label for="rbtDoku16-1">Ada</label>
                                                      <input id="rbtDoku16-2" class="magic-radio" type="radio" name="rbtDoku16"  onClick="setformDoku16(this.value)" value="Tidak" checked>
                                                      <label for="rbtDoku16-2">Tidak</label>
                                                  </div>
                                              </div>
                                              <div id="formDoku16">
                                                  <div class="col-md-5">
                                                      <input type="file" id="file_doku16" class="form-control" name="file_doku16"  />
                                                  </div>
                                              </div>
                                          </div>
                                      </div>

                                      <div class="col-sm-10">
                                          <div class="form-group">
                                              <label class="col-md-4 control-label" >Data Keuangan Penjamin (jika ada) *</label>
                                              <div class="col-md-4">
                                                  <div class="radio">
                                                      <input id="rbtDoku16-1" class="magic-radio" type="radio" name="rbtDoku16"  onClick="setformDoku16(this.value)" value="Ada">
                                                      <label for="rbtDoku16-1">Ada</label>
                                                      <input id="rbtDoku16-2" class="magic-radio" type="radio" name="rbtDoku16"  onClick="setformDoku16(this.value)" value="Tidak" checked>
                                                      <label for="rbtDoku16-2">Tidak</label>
                                                  </div>
                                              </div>
                                              <div id="formDoku16">
                                                  <div class="col-md-5">
                                                      <input type="file" id="file_doku16" class="form-control" name="file_doku16"  />
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                              </div>

                              <div class="col-sm-12 mar-ver">
                                  <div class="col-sm-4">
                                      <a class="btn btn-default btn-block mar-ver" href="{{ redirect()->back()->getTargetUrl()}}">Tutup Halaman ini</a>
                                  </div>
                                  <div class="col-sm-4">
                                  	  <button class="btn btn-primary btn-block mar-ver" type="submit">Simpan</button>
                                  </div>
                              </div>
                          </form>
                      </div>
                  </div>
              </div>
            </div>
          </div>
				@include('agent.sidebar')
			</div>
		@endforeach
	</div>
@endsection

@push('script')
  <script>
    $( document ).ready(function() {
      $('.dtpicker').datepicker({
        dateFormat : "dd/mm/yy"
      });
    $('.selectpicker').selectpicker('refresh');
    $('#formJenisPembiayaanBarang').show();
    $('#formJenisPembiayaanModalKerja').hide();
    $('#formJenisPembiayaanJasa').hide();
    });
    function setformDataJenisPembiayaan(val){
    if (val=='Barang') {
    $('#formJenisPembiayaanBarang').show();
    $('#formJenisPembiayaanModalKerja').hide();
    $('#formJenisPembiayaanJasa').hide();
    }else if (val=='Modal Kerja'){
    $('#formJenisPembiayaanBarang').hide();
    $('#formJenisPembiayaanModalKerja').show();
    $('#formJenisPembiayaanJasa').hide();
    }else if (val=='Jasa'){
    $('#formJenisPembiayaanBarang').hide();
    $('#formJenisPembiayaanModalKerja').hide();
    $('#formJenisPembiayaanJasa').show();
    }
    }
    function setformDoku1(val){
    if (val=='Ada') {
    $('#formDoku1').show();
    }else{
    $('#formDoku1').hide();
    }
    }
    function setformDoku2(val){
    if (val=='Ada') {
    $('#formDoku2').show();
    }else{
    $('#formDoku2').hide();
    }
    }
  </script>
@endpush
