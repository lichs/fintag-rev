<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForeignKeyPengajuanPuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pengajuan_pu', function($table){
            $table->foreign('request_id')->references('id')->on('request_agent');
            $table->foreign('agent_id')->references('id')->on('agents');
            $table->foreign('pelaku_usaha_id')->references('id')->on('pelaku_usaha');
            $table->foreign('jenis_usaha_id')->references('id')->on('jenis_usaha');
            $table->foreign('jenis_pengajuan_id')->references('id')->on('jenis_pengajuan');
            $table->foreign('jenis_kepemilikan_usaha_id')->references('id')->on('jenis_kepemilikan_usaha');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
