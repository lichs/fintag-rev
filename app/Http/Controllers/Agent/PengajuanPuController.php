<?php

namespace App\Http\Controllers\Agent;

use App\PengajuanPu;
use App\RequestAgent;
use App\IdentitasPengajuanPerorangan;
use App\Agent;
use App\PelakuUsaha;
use Illuminate\Support\Facades\View;
//use Illuminate\View\View;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PengajuanPuController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth');
      $this->middleware('role:agent');
  }

  public function getPersyaratanDokumenPage()
  {
    return view('agent.page.persyaratan_dokumen');
  }

  public function getProfileBadanUsahaPage()
  {
    return view('agent.page.profile_badan_usaha');
  }

  public function getProfileUsahaPage()
  {
    return view('agent.page.profile_usaha');
  }

  public function getProfileKeuanganPage()
  {
    return view('agent.page.profile_keuangan');
  }

  public function getProfilePembiayaan()
  {
    return view('agent.page.profile_pembiayaan');
  }

  public function getProfilePribadi($pengajuan_id ,$pelaku_usaha_id)
  {
    $pelaku_usaha = PelakuUsaha::where('id',$pelaku_usaha_id)->get();
    //dd($pelaku_usaha);
    return view('agent.page.profile_pribadi',['pelaku_usaha' => $pelaku_usaha, 'pengajuan_id' => $pengajuan_id]);
  }

  public function getPengajuanBadanUsahaPage()
  {
    return view('agent.page.pengajuan_badan_usaha');
  }

  public function getPengajuanPeroranganPage($id)
  {
    $pelaku_usaha_id = PengajuanPu::where('id', $id)->pluck('pelaku_usaha_id');

    return view('agent.page.pengajuan_perorangan')->with(['pelaku_usaha_id' => $pelaku_usaha_id, 'pengajuan_id' => $id]);
  }

  public function createPengajuanPerorangan($request_id)
  {
    $agents_id = RequestAgent::where('id', $request_id)->pluck('agent_id');
    $pelaku_usahas_id = RequestAgent::where('id', $request_id)->pluck('pelaku_usaha_id');

    foreach ($agents_id as $agent_id) {
        $agents_id = $agent_id;
      }

    foreach ($pelaku_usahas_id as $pelaku_usaha_id) {
      $pelaku_usahas_id = $pelaku_usaha_id;
    }

    $pengajuan_perorangan = PengajuanPu::create([
      'request_id' => $request_id,
      'agent_id' => $agent_id,
      'pelaku_usaha_id' => $pelaku_usaha_id,
      'jenis_pengajuan_id' => 1
    ]);

    $pelaku_usaha_id = PengajuanPu::where('id', $pengajuan_perorangan->id)->pluck('pelaku_usaha_id');

    return view('agent.page.pengajuan_perorangan')->with(['pelaku_usaha_id' => $pelaku_usaha_id,'pengajuan_id' => $pengajuan_perorangan->id]);
  }

  public function createPengajuanBadanUsaha($request_id)
  {
    $agents_id = RequestAgent::where('id', $request_id)->pluck('agent_id');
    $pelaku_usahas_id = RequestAgent::where('id', $request_id)->pluck('pelaku_usaha_id');

    foreach ($agents_id as $agent_id) {
        $agents_id = $agent_id;
    }

    foreach ($pelaku_usahas_id as $pelaku_usaha_id) {
        $pelaku_usahas_id = $pelaku_usaha_id;
    }

    $pengajuan_perorangan = PengajuanPu::create([
        'request_id' => $request_id,
        'agent_id' => $agent_id,
        'pelaku_usaha_id' => $pelaku_usaha_id,
        'jenis_pengajuan_id' => 2
    ]);

    return view('agent.page.pengajuan_perorangan');
  }

  public function submitProfilePribadi(Request $request)
  {
    $pengajuanPu = PengajuanPu::findOrFail($request->pengajuan_id);
    $cursor = $request->cursor;

    switch ($cursor) :
      case 1:
        return 'profil pribadi';
        break;
      case 2:
        return 'profil usaha';
        break;
      case 3:
        # code...
        break;
      case 4:
        # code...
        break;
  endswitch;

    if ($pengajuanPu->identitasPengajuanPerorangan()->get()->isEmpty()) :
        // Create data identitas pengajan perorangan.
        switch ($cursor) :
          case 1:
            return 'profil pribadi';
            $pengajuanPu->identitasPengajuanPerorangan()->create([
                'pengajuan_id' => $pengajuanPu->id,
                'ktp_image' => $request->ktp_image,
                'npwp' => $request->npwp,
                'npwp_image' => $request->npwp_image,
                'status_npwp' => $request->status_npwp,
                'status_pernikahan' => $request->status_pernikahan,
                'alamat_ktp' => $request->alamat_ktp,
                'alamat' => $request->alamat,
                'status_kepemilikan_rumah' => $request->status_kepemilikan_rumah,
                'lama_tahun_menempati_rumah' => $request->lama_tahun_menempati_rumah,
                'lama_bulan_menempati_rumah' => $request->lama_bulan_menempati_rumah,
                'status_kartu_keluarga' => $request->status_kartu_keluarga,
                'status_akta_nikah_cerai' => $request->status_akta_nikah_cerai,
                'status_akta_nikah_cerai_image' => $request->status_akta_nikah_cerai_image,
                'status_ktp_pasangan' => $request->status_ktp_pasangan,
                'ktp_pasangan_image' => $request->ktp_pasangan_image
            ]);
            break;
          case 2:
            return 'profil usaha';
            break;
          case 3:
            # code...
            break;
          case 4:
            # code...
            break;
      endswitch;


    else:
        // Update data dari identitas pengajan perorangan.
        //$identitasPengajuanPerorangan = IdentitasPengajuanPerorangan::where('pengajuan_id');
        $pengajuanPu->identitasPengajuanPerorangan()->create();
        $identitasPengajuanPerorangan->pengajuan_id = '';
        $identitasPengajuanPerorangan->ktp_image = '';
        $identitasPengajuanPerorangan->npwp = '';
        $identitasPengajuanPerorangan->npwp_image = '';
        $identitasPengajuanPerorangan->status_npwp = '';
        $identitasPengajuanPerorangan->status_pernikahan = '';
        $identitasPengajuanPerorangan->alamat_ktp = '';
        $identitasPengajuanPerorangan->alamat = '';
        $identitasPengajuanPerorangan->status_kepemilikan_rumah = '';
        $identitasPengajuanPerorangan->lama_tahun_menempati_rumah = '';
        $identitasPengajuanPerorangan->lama_bulan_menempati_rumah = '';
        $identitasPengajuanPerorangan->status_kartu_keluarga = '';
        $identitasPengajuanPerorangan->status_akta_nikah_cerai = '';
        $identitasPengajuanPerorangan->status_akta_nikah_cerai_image = '';
        $identitasPengajuanPerorangan->status_ktp_pasangan = '';
        $identitasPengajuanPerorangan->ktp_pasangan_image = '';
        $identitasPengajuanPerorangan->status_siup = '';
        $identitasPengajuanPerorangan->siup_image = '';
        $identitasPengajuanPerorangan->status_tdp = '';
        $identitasPengajuanPerorangan->tdp_image = '';
        $identitasPengajuanPerorangan->status_skdp_situ = '';
        $identitasPengajuanPerorangan->skdp_situ_image = '';
        $identitasPengajuanPerorangan->status_akte_pendirian = '';
        $identitasPengajuanPerorangan->akte_pendirian_image = '';
        $identitasPengajuanPerorangan->status_ijin_usaha = '';
        $identitasPengajuanPerorangan->ijin_usaha_image = '';
        $identitasPengajuanPerorangan->status_kontrak = '';
        $identitasPengajuanPerorangan->kontrak_image = '';
        $identitasPengajuanPerorangan->status_nasabah_bank = '';
        $identitasPengajuanPerorangan->rek_tabungan_image = '';
        $identitasPengajuanPerorangan->status_laporan_keuangan = '';
        $identitasPengajuanPerorangan->laporan_keuangan_image = '';
        $identitasPengajuanPerorangan->status_cash_flow = '';
        $identitasPengajuanPerorangan->cash_flow_image = '';
        $identitasPengajuanPerorangan->status_daftar_utang = '';
        $identitasPengajuanPerorangan->daftar_utang_image = '';
        $identitasPengajuanPerorangan->status_daftar_mesin = '';
        $identitasPengajuanPerorangan->daftar_alat_image = '';
        $identitasPengajuanPerorangan->berat_mesin = '';
        $identitasPengajuanPerorangan->status_daftar_penjamin = '';
        $identitasPengajuanPerorangan->daftar_penjamin_image = '';
        $identitasPengajuanPerorangan->status_daftar_keuangan_penjamin = '';
        $identitasPengajuanPerorangan->daftar_keuangan_penjamin_image = '';

    endif;
  }

  public function identifyCursorCreate($cursor)
  {
    if ($cursor = 1) {

    }
  }

  public function identifyCursorUpdate($cursor)
  {
    if ($cursor = 1) {

    }
  }
}
