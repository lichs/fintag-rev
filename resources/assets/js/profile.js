$(function(){

  $('[data-toggle="tooltip"]').tooltip();

  $('.select2').select2();

  //$('#no_telp').mask();

  $('#provinsi').on('change', function(){
      $.get("/agent/data/kabupaten/" + $(this).val(), function(data){
          $('#kabupaten').empty();

          $.each(data, function(key,value){
            $('#kabupaten').append($("<option></option>").attr("value", value.id).text(value.name))
          })
      })
  });

  $('#kabupaten').on('change', function(){
      $.get("/agent/data/kecamatan/" + $(this).val(), function(data){
          $('#kecamatan').empty();
          $.each(data, function(key,value){
            $('#kecamatan').append($("<option></option>").attr("value", value.id).text(value.name))
          })
      })
  });

  $('#kecamatan').on('change', function(){
      $.get("/agent/data/kelurahan/" + $(this).val(), function(data){
          $('#kelurahan').empty();

          $.each(data, function(key,value){
            $('#kelurahan').append($("<option></option>").attr("value", value.id).text(value.name))
          })
      })
  });


  $('#provinsi_ktp').on('change', function(){
      $.get("/agent/data/kabupaten/" + $(this).val(), function(data){
          $('#kabupaten_ktp').empty();

          $.each(data, function(key,value){
            $('#kabupaten_ktp').append($("<option></option>").attr("value", value.id).text(value.name))
          })
      })
  });

  $('#kabupaten_ktp').on('change', function(){
      $.get("/agent/data/kecamatan/" + $(this).val(), function(data){
          $('#kecamatan_ktp').empty();

          $.each(data, function(key,value){
            $('#kecamatan_ktp').append($("<option></option>").attr("value", value.id).text(value.name))
          })
      })
  });

  $('#kecamatan_ktp').on('change', function(){
      $.get("/agent/data/kelurahan/" + $(this).val(), function(data){
          $('#kelurahan_ktp').empty();

          $.each(data, function(key,value){
            $('#kelurahan_ktp').append($("<option></option>").attr("value", value.id).text(value.name))
          })
      })
  });

  $('.uploadImage').on('change', function(e){
		var filename = e.target.files[0].name;
		$('.uploadName').append('<p class="text-small">' + filename + '</p>');
	});

/*  $('#update').click(function(e){
      e.preventDefault();
      console.log('test');

      $.ajax({
          method: "PUT",
          dataType: "json",
          url: "/agent/profile/" + $('#id').val(),
          data:
          {
              '_token': $('meta[name="csrf-token"]').attr('content'),
              'id': $('#id').val(),
              'nama': $('#nama').val(),
              'no_telp': $('#no_telp').val(),
              'nik': $('nik').val()
          },
          success: function(data)
          {
              if (data.success == true)
              {
                $.niftyNoty({
                  type: 'info',
                  title: 'Message.',
                  message: 'Biodata Diri berhasil diubah',
                  container: 'floating',
                  timer: 5000
                });
              }
          }
      });
 });

 */

});
